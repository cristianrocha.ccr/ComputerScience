package com.chatclient.app;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class ChatRoom extends ActionBarActivity {


    public static final String NOTIFICATION = "com.chatclient.app";
    boolean mBounded;
    MyConnection mconn;
    //set data list
    public final ArrayList<String> list = new ArrayList<String>();
    /** Declaring an ArrayAdapter to set items to ListView */
    public ArrayAdapter<String> adapter;

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String string = bundle.getString(MyConnection.RESULT);

                if (string.equals("newmessage_chatroom")) {
                    String message = bundle.getString(MyConnection.MESSAGE);
                    String user = bundle.getString(MyConnection.USERNAME);

                    writeMessage(message, user);
                }
                else if (string.equals("newuser")) {

                }
            }

        }
    };

    public void writeMessage(String txt, String name){
        final ListView myListView = (ListView)findViewById(R.id.chat);

        list.add(name + ":" + "\n" + txt + "\n");


        myListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        myListView.setSelection(adapter.getCount() - 1);



    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);

        Intent intent = getIntent();
        String message = intent.getStringExtra("MESSAGE");
        //Set Values
        final TextView tv = (TextView) findViewById(R.id.messageName);
        final EditText et = (EditText) findViewById(R.id.textMessage);
        //Set textview to show the destiny
        tv.setText("Chat Room");

        //Log.i("RESULT", "TYPE" + type + "MESS" + message + "DES" + dest);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);

        Button button = (Button) findViewById(R.id.sendMessageButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double var1,var2;
                //if there is no gps data, fake gps
                var1 =  mconn.myLocation == null? mconn.myLocation.getLatitude():-37.907165;
                var2 =  mconn.myLocation == null? mconn.myLocation.getLongitude(): 145.138531;

                Log.i("MESSAGE", "to" + "name:"+et.getText().toString()+var1+"|"+var2);
                mconn.session = "scanning";
                mconn.sendMessage("chat|"+et.getText().toString()+"|"+mconn.myUserName+"|"+mconn.myId+"|"+var1+"|"+var2);
                writeMessage(et.getText().toString(), mconn.myUserName);
                et.setText("");
            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.chat_room, menu);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
    };
    ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(ChatRoom.this, "Service is disconnected", 1000).show();
            mBounded = false;
            mconn = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            Toast.makeText(ChatRoom.this, "Service is connected", 1000).show();
            mBounded = true;
            MyConnection.LocalBinder mLocalBinder = (MyConnection.LocalBinder)service;
            mconn = mLocalBinder.getServerInstance();
        }
    };

    protected void onResume() {

        super.onResume();
        Intent mIntent = new Intent(ChatRoom.this, MyConnection.class);
        bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE);
        registerReceiver(receiver, new IntentFilter(MyConnection.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(mConnection);
        unregisterReceiver(receiver);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
