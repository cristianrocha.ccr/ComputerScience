package com.chatclient.app;

import android.app.AlertDialog;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import io.socket.IOAcknowledge;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyConnection extends IntentService implements LocationListener {
    public static final String USERID = "userid";
    public static final String USERNAME = "username";
    public static final String MESSAGE = "message";

    public static final String RESULT = "result";
    public static final String NOTIFICATION = "com.chatclient.app";

    private WebSocketClient mWebSocketClient;
    public String myId = "notset";
    public String myUserName = "name";
    public String session = "session";
    protected LocationManager locationManager;
    public Location myLocation = null;
    public String status = "nodata";


    private  Context mContext;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute



    public Location getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                Log.d("TEST ERROR", "GPS Enabled");
                // no network provider is enabled
            } else {

                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        myLocation = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (myLocation != null) {
                            latitude = myLocation.getLatitude();
                            longitude = myLocation.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (myLocation == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            myLocation = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (myLocation != null) {
                                latitude = myLocation.getLatitude();
                                longitude = myLocation.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return myLocation;
    }


    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }



    //@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onHandleIntent(intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }



    IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("Location", "Loc" + location);
        myLocation = location;


    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude","status");
    }



    public class LocalBinder extends Binder {
        public MyConnection getServerInstance() {
            return MyConnection.this;
        }
    }



    public MyConnection() {
        super("MyConnection");
    }



   public void onCreate() {
        super.onCreate();
           //registerReceiver(receiver, new IntentFilter(MainActivity.NOTIFICATION));

    }





   public void sendMessage(String text){


       mWebSocketClient.send(text);



   }


    public WebSocketClient connectWeb(){


        URI uri;
        try {
            uri = new URI("ws://192.168.0.100:1337");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }


         mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");

                //mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
                //mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
            }

            @Override
            public void onMessage(String s) {
                String[] test = s.split("\\|");
                //mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);

                checkMessage(s);

            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i("Websocket", "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i("Websocket", "Error " + e.getMessage());
            }


        };
        mWebSocketClient.connect();


        return mWebSocketClient;
    }



    public void checkMessage(String message){

        String[] msg = message.split("\\|");
        Log.i("Message", "type " + msg[0]);
        //Set my userID
        if(msg[0].equals("userID")){// && myId.equals("notset")){

            myId = msg[1];
            myUserName = msg[2];
            Log.i("USERID", "NOTSET " + myId);
            Intent intent = new Intent(NOTIFICATION);
            intent.putExtra(RESULT, "ready");
            sendBroadcast(intent);



        }
        //List user online
        else if(msg[0].equals("userList") && !msg[1].equals(myId)){

            Log.i("USERID", "SET ");


            Location l = new Location("Client");
            l.setLatitude(Double.parseDouble(msg[3]));
            l.setLongitude(Double.parseDouble(msg[4]));
            Log.i("MYYY", "DISTANCE IS " + myLocation.distanceTo(l));
            if(myLocation.distanceTo(l)>1000000){
                Log.i("ENTREEEEEEEEEEEEEEEEEEEI", "UUHHHUL " + myLocation.distanceTo(l));


            }
            else  {
                Intent intent = new Intent(NOTIFICATION);
                intent.putExtra(RESULT, "newuser");
                intent.putExtra(USERID, msg[1]);
                intent.putExtra(USERNAME, msg[2]);
                sendBroadcast(intent);
            }

        }
        //Send data to the server
        else if(msg[0].equals("requestUser")){
            double var1,var2;
            //if there is no gps data, fake gps
            var1 =  myLocation == null? myLocation.getLatitude():-37.907165;
            var2 =  myLocation == null? myLocation.getLongitude(): 145.138531;

            sendMessage("request|" + myId + "|" + myUserName + "|" + var1 + "|" + var2);
            //sendMessage("request|" + myId + "|" + myUserName);
        }
        //Show message
        else if(msg[0].equals("message")){
            Log.i("MESSAGE ARRIVE",  msg[1] +  "FROM " + msg[2] + msg[3]);
            Intent intent = new Intent(NOTIFICATION);
            intent.putExtra(RESULT, "newmessage");
            intent.putExtra(MESSAGE, msg[1]);
            intent.putExtra(USERID, msg[2]);
            intent.putExtra(USERNAME, msg[3]);

            sendBroadcast(intent);
        }
        //Show message to chat room
        else if(msg[0].equals("sentChat")){
            Log.i("MESSAGE ARRIVE CHAT",  msg[1] +  "FROM " + msg[2] + msg[3]);
            //Just show the messages for the users around
            Location l = new Location("Client");
            l.setLatitude(Double.parseDouble(msg[4]));
            l.setLongitude(Double.parseDouble(msg[5]));
            Log.i("MY", "DISTANCE IS " + myLocation.distanceTo(l));
            if(myLocation.distanceTo(l)>1000000){
                Log.i("CHAAT", "UUHHHUL " + myLocation.distanceTo(l));
            }
            else {

                Intent intent = new Intent(NOTIFICATION);
                intent.putExtra(RESULT, "newmessage_chatroom");
                intent.putExtra(MESSAGE, msg[1]);
                intent.putExtra(USERID, msg[3]);
                intent.putExtra(USERNAME, msg[2]);

                sendBroadcast(intent);
            }
        }

    }


    public void chat(){
        Log.i("asdasdas", "Chat " );
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, "chat");
        sendBroadcast(intent);
        stopSelf();

    }



    public void askGPS(){
        Log.i("GPS", "GPS " );
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, "gps");
        sendBroadcast(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        getLocation();
        Log.d("Location:", "" + location);
        //locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 300000, 10, this);

//        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            //Do what you need if enabled...
  //          Log.i("GPS LIGADO", "UUHHHUL ");
    //    }else{
      //      Log.i("GPS DESLIGADO", "UUHHHUL ");
        //    askGPS();
        //}
        //Log.i("Websocket", "SDAAAAAAAAAAAA ");
        mWebSocketClient = connectWeb();

        int oi = mWebSocketClient.getReadyState();
           Log.i("Websocket", "ASFDFDFDFDFD " +  oi);


    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }


}
