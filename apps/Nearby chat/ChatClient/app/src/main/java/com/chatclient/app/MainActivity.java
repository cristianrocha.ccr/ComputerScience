package com.chatclient.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.LocationManager;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;





public class MainActivity extends Activity {
    private WebSocketClient mWebSocketClient;
    public SocketIO socket;

    public String message1;
    public static final String DESTINY = "null";
    public static final String TYPE = "null";
    public static final String MESSAGE = "null";
    public static final String STATUS = "result";
    public static final String NOTIFICATION = "com.chatclient.app";
    boolean mBounded;
    MyConnection mconn;

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String string = bundle.getString(MyConnection.RESULT);

                 if (string.equals("chat")) {
                    Toast.makeText(MainActivity.this,
                            "Send new message " + string,
                            Toast.LENGTH_LONG).show();
                   //sendMessage();

                }
                else if (string.equals("newuser")) {


                    String userID = bundle.getString(MyConnection.USERID);
                    String username = bundle.getString(MyConnection.USERNAME);

                    addUser(username, userID);
                    Toast.makeText(MainActivity.this,
                            "New User " + username + userID,
                            Toast.LENGTH_LONG).show();
                  //  final EditText et = (EditText) findViewById(R.id.editText);
                    //et.setText(username + " " + userID + " ");

                    //sendMessage();

                }
                 else if (string.equals("newmessage")) {
                     String message = bundle.getString(MyConnection.MESSAGE);
                     String userId = bundle.getString(MyConnection.USERID);
                     String userName = bundle.getString(MyConnection.USERNAME);
                     Toast.makeText(MainActivity.this,
                             "New message Received " ,
                             Toast.LENGTH_LONG).show();


                     chatInvite(message, userName, userId);
                     //  final EditText et = (EditText) findViewById(R.id.editText);
                     //et.setText(username + " " + userID + " ");

                     //sendMessage();

                 }



            }

        }
    };


    public void messageReceived(String message, String userName, String userId){
        Log.i("Message received", "Is" + message );
        Intent inte = new Intent(this, Message.class);
        inte.putExtra("TYPE", "received");
        inte.putExtra("MESSAGE", message);
        String dest = userId + " | " + userName;
        inte.putExtra("DESTINY",dest);
        startActivity(inte);



    }



    public void chatInvite(final String message,final String userName, final String userId){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("CHAT")
                .setMessage(userName + " Would like to open a chat with you")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Yes button clicked, do something
                        messageReceived(message, userName, userId);
                    }
                })
                .setNegativeButton("No", null)						//Do nothing on no
                .show();


    }


    public void askUser(final String item){
        String[] msg = item.split("\\ | ");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("CHAT")
                .setMessage("Do you want to open a chat with "+msg[2]+"?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Yes button clicked, do something
                            callMessage(item);
                    }
                })
                .setNegativeButton("No", null)						//Do nothing on no
                .show();

    }

    public void callMessage(String item){
        Intent inte = new Intent(this, Message.class);
        inte.putExtra("TYPE", "openmessage");
        inte.putExtra("DESTINY", item);
        startActivity(inte);
    }


    public void addUser(String name, String id){


        ListView myListView = (ListView)findViewById(R.id.listView);
        final ArrayList<String> list = new ArrayList<String>();

        list.add(id+" | " +name);

        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);

        myListView.setAdapter(adapter);


        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);


                Log.i("Message Chegueui", "Is" );
                askUser(item);
            }

        });
    }


    public void sendMessage(){
        final EditText et = (EditText) findViewById(R.id.editText);
        mconn.sendMessage(et.getText().toString());

    }

    public void openMessage(){

        final EditText et = (EditText) findViewById(R.id.editText);
        mconn.sendMessage(et.getText().toString());
        // add infos for the service which file to download and where to store

    }

    public void openChatRoom(){
        Intent inte = new Intent(this, ChatRoom.class);
        inte.putExtra("TYPE", "openchat");
        startActivity(inte);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Intent intent = getIntent();
        String name = intent.getStringExtra("NAME");
        Button button = (Button) findViewById(R.id.scan);
        Button button_chat = (Button) findViewById(R.id.chatRoom);
        TextView tv = (TextView) findViewById(R.id.textView2);
        tv.setText("Welcome, " + name);

        button.setOnClickListener(onClickListener);
        button_chat.setOnClickListener(onClickListener);


    }


    //Open chat room or open chat with a client
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.scan:
                    String id = mconn.myId;
                    mconn.session = "scanning";
                    mconn.sendMessage("scan|" + id);

                    break;
                case R.id.chatRoom:
                    openChatRoom();
                    break;

            }

        }
    };


    @Override
    protected void onStart() {
        super.onStart();
    };
    ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(MainActivity.this, "Service is disconnected", 1000).show();
            mBounded = false;
            mconn = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            Toast.makeText(MainActivity.this, "Service is connected", 1000).show();
            mBounded = true;
            MyConnection.LocalBinder mLocalBinder = (MyConnection.LocalBinder)service;
            mconn = mLocalBinder.getServerInstance();
        }
    };

    protected void onResume() {

        super.onResume();
        Intent mIntent = new Intent(MainActivity.this, MyConnection.class);
        bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE);
        registerReceiver(receiver, new IntentFilter(MyConnection.NOTIFICATION));

     }


    @Override
    protected void onPause() {
        super.onPause();
        unbindService(mConnection);
        unregisterReceiver(receiver);
    }



    }






