package com.chatclient.app;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class init extends ActionBarActivity {
    public static final String STATUS = "result";
    public static final String NAME = "name";
    boolean mBounded;
    MyConnection mconn;
    public Intent mIntent;
    public  Intent intent;
    public String name;

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String string = bundle.getString(MyConnection.RESULT);
                Log.i("asdasdas", "Is" + string);
                if (string.equals("ready")) {
                    callMain();
                }
                else if (string.equals("gps")) {


                    showSettingsAlert();
                    //  final EditText et = (EditText) findViewById(R.id.editText);
                    //et.setText(username + " " + userID + " ");

                    //sendMessage();

                }


            }


        }
    };

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public void callMain(){
        final EditText et = (EditText) findViewById(R.id.editText);
        Intent inte = new Intent(this, MainActivity.class);
        inte.putExtra("NAME",name);
        startActivity(inte);

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);


        intent = new Intent(this, MyConnection.class);
        startService(intent);


        final EditText et = (EditText) findViewById(R.id.editText);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = et.getText().toString();
                mconn.sendMessage(et.getText().toString());
                et.setText("");


            }


        });
    }



        @Override
        protected void onStart() {
            super.onStart();



        };
        ServiceConnection mConnection = new ServiceConnection() {

            public void onServiceDisconnected(ComponentName name) {
                Toast.makeText(init.this, "Service is disconnected", 1000).show();
                mBounded = false;
                mconn = null;
            }

            public void onServiceConnected(ComponentName name, IBinder service) {
                Toast.makeText(init.this, "Service is connected", 1000).show();
                mBounded = true;
                MyConnection.LocalBinder mLocalBinder = (MyConnection.LocalBinder)service;
                mconn = mLocalBinder.getServerInstance();
            }
        };

    protected void onResume() {
        super.onResume();
        mIntent = new Intent(this, MyConnection.class);
        bindService(mIntent, mConnection, BIND_AUTO_CREATE);
        registerReceiver(receiver, new IntentFilter(MyConnection.NOTIFICATION));
    }

    @Override
    protected void onPause() {

        super.onPause();

        unbindService(mConnection);
        unregisterReceiver(receiver);
    }

}
