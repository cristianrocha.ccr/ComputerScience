// chat-server.js

"use strict";

// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'node-chat';

// Port where we'll run the websocket server
var webSocketsServerPort = 1337;

// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');

/**
 * Global variables
 */
// entire message history
var history = new Array();
// list of currently connected clients (users)
var clients = new Array();


/**
 * HTTP server
 */
var server = http.createServer(function(request, response) {
    // Not important for us. We're writing WebSocket server, not HTTP server
});
server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. To be honest I don't understand why.
    httpServer: server
});
var num = -1;
var scanRequest;
// This callback function is called every time someone tries to connect to the WebSocket server
wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');

    // accept connection
    var connection = request.accept(null, request.origin); 
    // we need to know client index to remove them on 'close' event
    var index = clients.push(connection) - 1;
    var userName = false;
    

    console.log((new Date()) + ' Connection accepted.');
    num = num + 1;	
 
    
    
    var idDest;
    // user sent some message
    connection.on('message', function(message) {
	//Send request for all clients to return their data	
	if(message.utf8Data.split('|')[0] == 'scan'){
		console.log((new Date()) + ' Sending location requests ');
		scanRequest = parseInt(message.utf8Data.split('|')[1]);
		console.log(' ID ---> ' + scanRequest);
		for (var i=0; i < clients.length; i++) {
			if(i!=scanRequest){
		      		clients[i].sendUTF('requestUser');
			}
		}
	}
	//Send the data for the client which made the request
	else if(message.utf8Data.split('|')[0] == 'request'){
		console.log((new Date()) + ' Location request received ');
		console.log('USERID___>' + message.utf8Data.split('|')[2] + 'NAME___>' +message.utf8Data.split('|')[1]);
		clients[scanRequest].sendUTF('userList|' + message.utf8Data.split('|')[1] + '|' + message.utf8Data.split('|')[2]+ '|' + message.utf8Data.split('|')[3]+ '|' + message.utf8Data.split('|')[4]);

			}
	//Send the message for the destiny
	else if(message.utf8Data.split('|')[0] == 'message'){
		idDest = parseInt(message.utf8Data.split('|')[1]);
		console.log((new Date()) + ' Sending Message to '+ idDest + message.utf8Data.split('|')[2] + message.utf8Data.split('|')[3]);
		//send to the destiny message + src_id + src_name
		clients[idDest].sendUTF('message|' + message.utf8Data.split('|')[2]+'|'+message.utf8Data.split('|')[4]+'|'+message.utf8Data.split('|')[3]);
		
	}
	//Send the message for a chat room
	else if(message.utf8Data.split('|')[0] == 'chat'){
		var sender = parseInt(message.utf8Data.split('|')[3]);
		console.log((new Date()) + ' Sending Message to chat room from '+ sender + ' Message: '+ message.utf8Data.split('|')[2] + message.utf8Data.split('|')[3]);
		//send to the chat room message + src_name + src_id 
		for (var i=0; i < clients.length; i++) {
			if(i!=sender){
		      		clients[i].sendUTF('sentChat|'+message.utf8Data.split('|')[1]+'|'+message.utf8Data.split('|')[2]+'|'+sender+'|'+message.utf8Data.split('|')[4]+'|'+message.utf8Data.split('|')[5]);
			}
		}
	}
	//Begin connection
	else{
		if (message.type === 'utf8') { // accept only text
		    if (userName === false) { // first message sent by user is their name
		        // remember user name
		        userName = message.utf8Data;
		        console.log((new Date()) + ' User is known as: ' + userName);
			//Send user ID for the new client
			console.log((new Date()) + ' Sending ids ' + num);
		        clients[num].sendUTF('userID|' + num + '|' + message.utf8Data);
		    } 
	   }	
        }

    	
    });

    // user disconnected
    connection.on('close', function(connection) {
        if (userName !== false) {
            console.log((new Date()) + " Peer " + connection.remoteAddress + " disconnected.");
            // remove user from the list of connected clients
            clients.splice(index, 1);
            }
    });

});
