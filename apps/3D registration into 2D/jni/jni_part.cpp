#include <jni.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <android/log.h>
#include <iostream>
#include <fstream>
#include <cstddef>
#include <opencv2/imgproc/imgproc.hpp>
using namespace std;
using namespace cv;

extern "C" {
JNIEXPORT void JNICALL Java_com_example_detection_MainActivity_FindFeatures(JNIEnv*, jobject, jlong addrGray, jlong addrRgba, jlong descriptors_object, jlong keypoints1, jlong img1, jlong test);

JNIEXPORT void JNICALL Java_com_example_detection_MainActivity_FindFeatures(JNIEnv*, jobject, jlong addrGray, jlong addrRgba, jlong descriptors_object, jlong keypoints1, jlong img1, jlong test)
{
	// Create access to the file
	//cv::FileStorage file("/storage/emulated/0/mat.txt", cv::FileStorage::WRITE);

	  //ofstream myfile;
	  //myfile << descriptors_object;
	  //myfile.open ("mat.txt");
	  //myfile.close();

	// Write myMat to file!
	//file << "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa";


/*
    Mat& mGr  = *(Mat*)addrGray;
    Mat& mRgb = *(Mat*)addrRgba;
    vector<KeyPoint> v;

    FastFeatureDetector detector(50);
    detector.detect(mRgb, v);

    for( unsigned int i = 0; i < v.size(); i++ )
    {
        const KeyPoint& kp = v[i];
        circle(mRgb, Point(kp.pt.x, kp.pt.y), 10, Scalar(255,0,0,255));
    }
   */

	Mat& img_object  = *(Mat*)addrGray;
    Mat& img_scene = *(Mat*)addrRgba;
    Mat& obj = *(Mat*)descriptors_object;
    Mat& img_obj = *(Mat*)img1;
	Mat& key_obj = *(Mat*)keypoints1;
	Mat* t = (Mat*)test;
	//vector<DMatch>* t = (vector<DMatch>*)test;

	//vector<KeyPoint>& keypoints_object = *(vector<KeyPoint>*)keypoints1;



	vector<KeyPoint>  keypoints_scene, keypoints_object;
	 img_scene.convertTo(img_scene, CV_8UC3);
	 cvtColor(img_scene, img_scene, CV_BGRA2BGR);


	 img_obj.convertTo(img_obj, CV_8UC3);
	 cvtColor(img_obj, img_obj, CV_BGRA2BGR);



    FastFeatureDetector detector(100);
    detector.detect( img_scene, keypoints_scene );


    detector.detect(img_obj, keypoints_object);


    //-- Step 2: Calculate descriptors (feature vectors)
    OrbDescriptorExtractor extractor;
    Mat descriptors_scene, descr_ob;
    extractor.compute( img_scene, keypoints_scene, descriptors_scene );
    extractor.compute( img_obj, keypoints_object, descr_ob);




    Mat teste;
    BFMatcher matcher;
    vector< DMatch > matches;


    descriptors_scene.convertTo(descriptors_scene, CV_32F);
    descr_ob.convertTo(descr_ob, CV_32F); // make sure it's CV_32F




    //string matAsString (descriptors_object.begin<unsigned char>(), descriptors_object.end<unsigned char>());
    //const char *t =  matAsString.c_str();


    //__android_log_write(ANDROID_LOG_INFO, "CCCCCCCCCCCCCCCC", t);

    if (obj.cols == descriptors_scene.cols)
    {
    	__android_log_write(ANDROID_LOG_INFO, "CCCCCCCCCCCCCCCC", "I am in");
    	matcher.match( descr_ob, descriptors_scene, matches );


    double max_dist = 0; double min_dist = 100;

    //-- Quick calculation of max and min distances between keypoints
    for( int i = 0; i < descr_ob.rows; i++ )
    { double dist = matches[i].distance;
      if( dist < min_dist ) min_dist = dist;
      if( dist > max_dist ) max_dist = dist;
    }

      std::vector< DMatch > good_matches;

       for( int i = 0; i < descr_ob.rows; i++ )
       { if( matches[i].distance < 3*min_dist )
          { good_matches.push_back( matches[i]); }
       }



      // *t = matches;

       Mat img_matches;
       //if(keypoints_scene.size() <= keypoints_object.size()){

       //if(keypoints_scene.size() <= keypoints_object.size()){
       drawMatches( img_obj, keypoints_object, img_scene, keypoints_scene,
                  good_matches, img_matches);

       img_scene.convertTo(img_matches, CV_8UC3);
       cvtColor(img_matches, img_matches, COLOR_BGR2RGBA);
       *t = img_matches;


      //cvtColor(*t, *t, COLOR_RGB2RGBA);



       //t->release();

       //t = NULL;



       //*t = img_matches;


       /*}
       else if(keypoints_scene.size() >= keypoints_object.size()){
    	   drawMatches(  img_obj, keypoints_object, img_scene, keypoints_scene,
    	                     good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
    	                       vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );



       }*/



       for( unsigned int i = 0; i < keypoints_scene.size(); i++ )
       {
           const KeyPoint& kp = keypoints_scene[i];
           circle(img_scene, Point(kp.pt.x, kp.pt.y), 10, Scalar(255,0,0,255));
       }




    }

















  /*  Mat& img_object  = *(Mat*)addrGray;
    Mat& img_scene = *(Mat*)addrRgba;

    int minHessian = 400;
    vector<KeyPoint> v;
    vector<KeyPoint> keypoints_object, keypoints_scene;


    FastFeatureDetector detector( minHessian );



    //detector.detect( img_object, keypoints_object );
    detector.detect( img_scene, keypoints_scene );




  //  FastFeatureDetector detector(50);
   // detector.detect(mGr, v);

    //-- Step 2: Calculate descriptors (feature vectors)
     OrbDescriptorExtractor extractor;
     Mat descriptors_object, descriptors_scene;
     //extractor.compute( img_object, keypoints_object, descriptors_object );
    // extractor.compute( img_scene, keypoints_scene, descriptors_scene );

     //-- Step 3: Matching descriptor vectors using FLANN matcher

     /*FlannBasedMatcher matcher;
     vector< DMatch > matches;
     matcher.match( descriptors_object, descriptors_scene, matches );

     double max_dist = 0; double min_dist = 100;

     //-- Quick calculation of max and min distances between keypoints
     for( int i = 0; i < descriptors_object.rows; i++ )
     { double dist = matches[i].distance;
       if( dist < min_dist ) min_dist = dist;
       if( dist > max_dist ) max_dist = dist;
     }

     //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
     std::vector< DMatch > good_matches;

     for( int i = 0; i < descriptors_object.rows; i++ )
     { if( matches[i].distance < 3*min_dist )
        { good_matches.push_back( matches[i]); }
     }

     Mat img_matches;
     drawMatches( img_object, keypoints_object, img_scene, keypoints_scene,
                  good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                  vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );


    for( unsigned int i = 0; i < keypoints_scene.size(); i++ )
    {
        const KeyPoint& kp = keypoints_scene[i];
        circle(img_scene, Point(kp.pt.x, kp.pt.y), 10, Scalar(255,0,0,255));
    }
*/

}
}
