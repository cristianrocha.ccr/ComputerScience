package com.example.detection;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Scalar;
import org.opencv.features2d.*;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity  extends Activity implements CvCameraViewListener2{
    private MenuItem             mItemSwitchCamera = null;
    private boolean              mIsJavaCamera = true;
	private CameraBridgeViewBase   mOpenCvCameraView;
	private Mat                    mRgba;
	private Mat                    mGray;
	Mat img1 = Highgui.imread("/storage/emulated/0/DCIM/Camera/20150105_134208.jpg");
	Mat descriptors1 = new Mat();
	MatOfKeyPoint keypoints1 = new MatOfKeyPoint();
	Mat test = new Mat(); 
    protected static final String TAG = "CAMERA";
    

    static {
        if (!OpenCVLoader.initDebug()) {
        	Log.i(TAG, "{RPPPPPPPPPPPPPPPPPDSASDSDASSDSAD");
            // Handle initialization error
        }
        else{
        	System.loadLibrary("mixed_sample");
        	Log.i(TAG, "{UHUUUL");
        	
        }
    }
    
    
     
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
					mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_main);
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.tutorial2_activity_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        if (!OpenCVLoader.initDebug()) {
            Log.e(TAG, "Cannot connect to OpenCV Manager");
            
        } else {
        	Log.e(TAG, "CONNECTED");
        	mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);      
        }
		detector();
		
		
		
	}

	
	
	public void detector(){

		FeatureDetector detector = FeatureDetector.create(FeatureDetector.ORB);
		DescriptorExtractor descriptor = DescriptorExtractor.create(DescriptorExtractor.ORB);
		DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
		
		
		//first image
		 

		 
		 detector.detect(img1, keypoints1);
		 descriptor.compute(img1, keypoints1, descriptors1);
		 
		 
		 Log.i(TAG, "KEYPOINT");
		 String dump = keypoints1.dump();
		 Log.i(TAG, dump);
		 Log.i(TAG, "Descriptor");
		 String dump1 = descriptors1.dump();
		 Log.i(TAG, dump1);

		 
		 
		 /*Log.i(TAG, "CHEGUUUUUUUUUUEI");
		 Mat featuredImg = new Mat();		 
		 Scalar kpColor = new Scalar(255,159,10);//this will be color of keypoints
		 //featuredImg will be the output of first image
		 Features2d.drawKeypoints(img1, keypoints1, featuredImg , kpColor, 0);
		 //featuredImg will be the output of first image
		 Features2d.drawKeypoints(img1, keypoints1, featuredImg , kpColor, 0);		
		 Log.i(TAG, "C2222222222222");

		 // convert to bitmap:
	        Bitmap bm = Bitmap.createBitmap(featuredImg.cols(), featuredImg.rows(),Bitmap.Config.ARGB_8888);
	        Utils.matToBitmap(featuredImg, bm);
*/
	        // find the imageview and draw it!
	        //ImageView iv = (ImageView) findViewById(R.id.imageView1);
	        //iv.setImageBitmap(bm);
		 
		
	}
	
	
	
	
	@Override
	public void onResume()
	{
	    super.onResume();
	    //OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this, mLoaderCallback);
	}

	@Override
	public void onCameraViewStarted(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCameraViewStopped() {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void onPause(){
	   super.onPause();
	   if (mOpenCvCameraView != null)
	       mOpenCvCameraView.disableView();
    }
	
	 public void onDestroy() {
	     super.onDestroy();
	     if (mOpenCvCameraView != null)
	         mOpenCvCameraView.disableView();
	 }

	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		// TODO Auto-generated method stub
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();
        FindFeatures(mGray.getNativeObjAddr(), mRgba.getNativeObjAddr(), descriptors1.getNativeObjAddr(), keypoints1.getNativeObjAddr(), img1.getNativeObjAddr(), test.getNativeObjAddr());
        //Imgproc.resize(test, test, mRgba.size());
       
        /*
        //Highgui.imwrite("/storage/emulated/0/DCIM/Camera/result_match.jpeg", test); 
        // Log.i("desc:"  , test.dump());
        FeatureDetector detector = FeatureDetector.create(FeatureDetector.FAST);
		DescriptorExtractor descriptor = DescriptorExtractor.create(DescriptorExtractor.ORB);
		DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);

		
		Mat img = inputFrame.rgba();
		Mat descriptors = new Mat();
		MatOfKeyPoint keypoints = new MatOfKeyPoint();
		 

		
		
		
		
		
		
		 //String dump1 = matches.dump();
		 //Log.i(TAG, dump1);
		
		 
		 
		 Imgproc.cvtColor(img, img, Imgproc.COLOR_BGRA2BGR);
		 Imgproc.cvtColor(img1, img1, Imgproc.COLOR_BGRA2BGR);
		 detector.detect(img, keypoints);
		 descriptor.compute(img, keypoints, descriptors);
		 
		 //matcher should include 2 different image's descriptors
		 MatOfDMatch  matches = new MatOfDMatch();  
		 matcher.match(descriptors,descriptors1,matches);

					
		 Scalar RED = new Scalar(255,0,0);
		 Scalar GREEN = new Scalar(0,255,0);
		 //output image
		 Mat outputImg = new Mat();
		 MatOfByte drawnMatches = new MatOfByte();
		 //this will draw all matches, works fine
		
		 //Features2d.drawKeypoints(img, keypoints, outputImg);
		 
		 
		  Features2d.drawMatches(img, keypoints, img1, keypoints1, matches, 
		 outputImg, GREEN, RED,  drawnMatches, Features2d.NOT_DRAW_SINGLE_POINTS);
		  
		  Imgproc.cvtColor(outputImg, img, Imgproc.COLOR_RGB2RGBA);
		  */
		
		return test;
		
	}
	public native void FindFeatures(long matAddrGr, long matAddrRgba, long desc, long kpoints, long img1, long test);
}
