#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "porto.h"
#include "disco.h"

void FilaVaziaTrem(TipoFilaTrem *Fila)
{
	Fila->Frente=(TipoApontadorFilaTrem)malloc(sizeof(TipoCelulaFilaTrem));
	Fila->Tras=Fila->Frente;
	Fila->Frente->Prox=NULL;
}

void FilaVaziaCam(TipoFilaCam *Fila)
{
	Fila->Frente=(TipoApontadorFilaCam)malloc(sizeof(TipoCelulaFilaCam));
	Fila->Tras=Fila->Frente;
	Fila->Frente->Prox=NULL;
}

int FilasVazias()
{
	return (FilaTrem.Frente == FilaTrem.Tras && FilaCam.Frente == FilaCam.Tras);
}
int VaziaFilaTrem(TipoFilaTrem Trem)
{
	return (FilaTrem.Frente == FilaTrem.Tras);
}
int VaziaFilaCam(TipoFilaCam Cam)
{
	return (FilaCam.Frente == FilaCam.Tras);
}

void Carrega()
{
	TipoItemLista Trem;
	TipoItemLista Cam;	
    int tempoEntrada,
	tempoAduana;
	FilaVaziaTrem((&FilaTrem));
	FilaVaziaCam((&FilaCam));
	char nome[5];
	char tipo[5]; 	
	int ok;

	do{
		ok = scanf("%d",&tempoEntrada);
		if(ok==1){
			scanf("%d",&tempoAduana);	
			scanf("%s",nome);
			scanf("%s",tipo);
			
			if(strcmp(tipo,"TRE")==0){
				Trem.chave=0;
				Trem.tempoEntrada = tempoEntrada;
				Trem.tempoAduana = tempoAduana;
				strcpy(Trem.nome,nome); 
				strcpy(Trem.tipo,tipo);	
				EnfileiraTrem(Trem,&FilaTrem);
			}
			else{
				
				Cam.chave=0;
				Cam.tempoEntrada = tempoEntrada;
				Cam.tempoAduana = tempoAduana;

				strcpy(Cam.nome,nome);
				strcpy(Cam.tipo,tipo);	

				EnfileiraCam(Cam,&FilaCam);
			}			
		}	
	}while(ok==1);
}

int EnfileiraTrem (TipoItemLista x, TipoFilaTrem *Fila)
{
	int erro;
    Fila->Tras->Prox = (TipoApontadorFilaTrem)malloc(sizeof(TipoCelulaFilaTrem));
    if (Fila->Tras->Prox == NULL)
    {
        printf ("Erro na alocação de trem\n");
        return erro=TRUE;        
    }
    Fila->Tras = Fila->Tras->Prox;
    Fila->Tras->Prox = NULL;
    Fila->Tras->Item = x;
    return erro=FALSE;    
}

int EnfileiraCam (TipoItemLista x, TipoFilaCam *Fila)
{
	int erro;
    Fila->Tras->Prox = (TipoApontadorFilaCam)malloc(sizeof(TipoCelulaFilaCam));
    if (Fila->Tras->Prox == NULL)
    {
        printf ("Erro na alocação de Cam\n");
        return erro=TRUE;        
    }
    Fila->Tras = Fila->Tras->Prox;
    Fila->Tras->Prox = NULL;
    Fila->Tras->Item = x;
    return erro=FALSE;    
}

void Desenfileira(TipoItemLista Item)
{
	if (strcmp(Item.tipo,"TRE")==0)
	{
		DesenfileiraTrem(&FilaTrem);
		return;
	}	
	DesenfileiraCam(&FilaCam);
}	
				
void DesenfileiraTrem(TipoFilaTrem *Fila)
{ 
    TipoApontadorFilaTrem q;
        
    if(VaziaFilaTrem(*Fila))
    {
        printf("ERRO, Não é possível desenfileirar");
        return;
    }
    q = Fila->Frente;
    Fila->Frente=Fila->Frente->Prox;

    free(q);
}

void DesenfileiraCam(TipoFilaCam *Fila)
{ 
	
    TipoApontadorFilaCam q;
        
    if(VaziaFilaCam(*Fila))
    {
        printf("ERRO, Não é possível desenfileirar");
        return;
    }
    q = Fila->Frente;
    Fila->Frente=Fila->Frente->Prox;

    free(q);
}


void imprimeEntra(TipoItemLista Lista,int tempo){

	printf("entra %d %s \n",tempo,Lista.nome);
	
		
}

void imprimeCarrega(TipoItemLista Lista,int tempo){

	printf("carrega %d %s \n",tempo,Lista.nome);
	
}	
	
