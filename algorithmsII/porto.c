#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "porto.h"
#include "lista.h"	
#include "container.h"	
	
int main()
{
	int tempo,guindaste;
	TipoItemLista Item;
	
	Carrega();
	ZeraLista();
	
	guindaste=0;
	tempo = 0;
	
		
	while(!FilasVazias() || !ListaVazia() )
	{	
		
		
		if(!FilasVazias() && !PatioCheio() && guindaste==0 && CarregaContainer(&Item,tempo))
		{
			if (strcmp(Item.tipo,"TRE")==0)
			{
				imprimeEntra(Item,tempo);
				Desenfileira(Item);
				colocaNaLista(Item);
				guindaste=3;
			}	
		}
		
		
		if(!ListaVazia() && guindaste==0 && DeuTempoAduana(&Item,tempo))
		{
			imprimeCarrega(Item,tempo);
			tiraDaLista(Item);
			guindaste=3;	
		}
		else
		{
			if(!FilasVazias() && !PatioCheio() && guindaste==0 && CarregaContainer(&Item,tempo))
			{
				imprimeEntra(Item,tempo);
				Desenfileira(Item);
				colocaNaLista(Item);
				guindaste=3;
			}	
		}
		
		tempo++;
		if (guindaste>0)
			guindaste--;
		
	} 	
	
}
