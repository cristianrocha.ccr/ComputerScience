#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "porto.h"
#include "lista.h"

int CarregaContainer(TipoItemLista *Item,int tempo) 
{
 	int ok;
 	
 	
 	if((FilaTrem.Frente->Prox != NULL) && FilaTrem.Frente->Prox->Item.tempoEntrada <= tempo)
	{	
		*Item = FilaTrem.Frente->Prox->Item;
		(*Item).tempoEntrada= tempo;
		(*Item).chave=1;		
		return ok=TRUE;
	}
	else if ((FilaCam.Frente->Prox != NULL) && FilaCam.Frente->Prox->Item.tempoEntrada <= tempo)
 	{
		*Item = FilaCam.Frente->Prox->Item;
		(*Item).tempoEntrada= tempo;
		(*Item).chave=1;
		return ok=TRUE;
 	}
 	else
 	{
		return FALSE;
	}	
 }

int DeuTempoAduana(TipoItemLista *Item, int tempo) 
{
	int i,ok;
 	
 	i=0;
 	for (i=0;i<5;i++)
 	{	
		
		if(Lista[i].chave!=0)
		{

		
			if((Lista[i].tempoAduana + Lista[i].tempoEntrada + 3) <= tempo)
			{
				*Item=Lista[i];
				return ok=TRUE;
			}	
		}
	}
	return ok=FALSE;
}		
