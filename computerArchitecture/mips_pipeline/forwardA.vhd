library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.p_MI0.all;

entity forwardA is 
	port(
		MEM_EscreveReg, WB_EscreveReg : in std_logic;
		WB_RegDest, MEM_RegDest, EX_rs: in std_logic_vector (4  downto 0);
		saida: out  std_logic_vector (1 downto 0)
	);
end forwardA;
		
architecture arq_forwardA of forwardA is
begin 
	Forward: process (MEM_EscreveREG, WB_EscreveREG, WB_RegDest, MEM_RegDest, EX_rs)
	begin
		if (( WB_EscreveReg = '1') 	and
			(WB_RegDest /= "00000") and
			(MEM_RegDest /= EX_rs)	and
			(WB_RegDest = EX_rs)) 	then
				saida <= "10";
	
		elsif (( MEM_EscreveReg = '1') 	and
			(MEM_RegDest /= "00000")and
			(MEM_RegDest = EX_rs)) 	then
				saida <= "01";

		else
				saida <= "00";
		
		end if;

	end process;

end arq_forwardA;
