library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.p_MI0.all;

entity forwardB is 
	port(
		MEM_EscreveReg, WB_EscreveReg : in std_logic;
		WB_RegDest, MEM_RegDest, EX_rt: in std_logic_vector (4  downto 0);
		saida: out  std_logic_vector (1 downto 0)
	);
end forwardB;
		
architecture arq_forwardB of forwardB is
begin 
	Forward: process (MEM_EscreveREG, WB_EscreveREG, WB_RegDest, MEM_RegDest, EX_rt)
	begin
		if (( WB_EscreveReg = '1') 	and
			(WB_RegDest /= "00000") and
			(MEM_RegDest /= EX_rt)	and
			(WB_RegDest = EX_rt)) 	then
				saida <= "10";
	
		elsif (( MEM_EscreveReg = '1') 	and
			(MEM_RegDest /= "00000")and
			(MEM_RegDest = EX_rt)) 	then
				saida <= "01";

		else
				saida <= "00";
		
		end if;

	end process;

end arq_forwardB;
