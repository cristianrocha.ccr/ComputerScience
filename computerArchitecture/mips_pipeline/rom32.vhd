library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use work.p_MI0.all;



entity rom32 is
	port (
		address: in reg32;
		data_out: out reg32
	);
end rom32;

architecture arq_rom32 of rom32 is

	signal mem_offset: std_logic_vector(5 downto 0);
	signal address_select: std_logic;
	

begin
	mem_offset <= address(7 downto 2);
        
	add_sel: process(address)
	begin
		if address(31 downto 8) = x"000000" then 	
			address_select <= '1';
		else
			address_select <= '0';
		end if;
	end process;

	access_rom: process(address_select, mem_offset)
	begin
		if address_select = '1' then
			case mem_offset is
				when 	"000000" => data_out <= "001000" & "00000" & "01010" & x"000a"; -- addi $10, $0, 10
				when 	"000001" => data_out <= "001000" & "01011" & "01011" & x"000a"; -- addi $11, $11, 10
				when 	"000010" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"000011" => data_out <= "000100" & "01010" & "01011" & x"0005"; -- beq $10, $11, 5
				when 	"000100" => data_out <= "001000" & "00000" & "00110" & x"000a"; -- addi $6, $0, 10
				when 	"000101" => data_out <= "000011" & "00000000000000000000001100"; -- jal 12
				when 	"000110" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"000111" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"001000" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"001001" => data_out <= "001000" & "01010" & "01010" & x"0005"; -- addi $10, $10, 5
				when 	"001010" => data_out <= "001000" & "01011" & "01011" & x"0005"; -- addi $11, $11, 5
				when 	"001011" => data_out <= "000100" & "01010" & "01011" & x"0008"; -- beq $10, $11, 8
				when 	"001100" => data_out <= "101011" & "00110" & "00101" & x"0000"; -- sw $5, 0($6)
				when 	"001101" => data_out <= "100101" & "00110" & "00111" & x"0000"; -- lw $7, 0($6)
				when 	"001110" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"001111" => data_out <= "001000" & "00111" & "00111" & x"0001"; -- addi $7, $7, 1
				when 	"010000" => data_out <= "000000" & "00111" & "00000" & "00101" & "00000" & "100000"; -- add $5, $7, $0
				when 	"010001" => data_out <= "000000" & "01010" & "00111" & "01001" & "00000" & "101010"; -- slt $9, $10, $7
				when 	"010010" => data_out <= "000100" & "01001" & "01011" & x"0001"; -- beq $9, $11, 1
				when 	"010011" => data_out <= "000011" & "00000000000000000000001100"; -- jal 12
				when 	"010100" => data_out <= "001000" & "00000" & "00010" & x"000a"; -- addi $2, $0, 10
				when 	"010101" => data_out <= "101011" & "00110" & "00010" & x"0000"; -- sw $2, 0($6)
				when 	"010110" => data_out <= "100101" & "00110" & "00101" & x"0000"; -- lw $5, 0($6)
				when 	"010111" => data_out <= "001000" & "00101" & "00010" & x"0000"; -- addi $2, $5, 0
				when 	"011000" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"011001" => data_out <= "001000" & "00010" & "00010" & x"0002"; -- addi $2, $2, 2
				when 	others  => data_out <= (others => 'X');
			end case;
    		end if;
  	end process; 

end arq_rom32;
