library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.p_MI0.all;

entity UDH is 
	port(EX_MemRead: in std_logic;
		 EX_rt, ID_rs, ID_rt, WB_RegRd: in std_logic_vector(4 downto 0);
		 stall: out std_logic
	);
end UDH;
		
architecture arq_UDH of UDH is
begin 
	Riscos: process (EX_MemRead, EX_rt, ID_rs, ID_rt, WB_RegRd)
	begin
	
		if  (EX_MemRead = '1') and
			((EX_rt = ID_rs) or
			 (EX_rt = ID_rt)) then
			stall <= '1';
		else
			stall <= '0';
		end if;

	end process;

end arq_UDH;
