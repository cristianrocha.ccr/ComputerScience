library IEEE;
use IEEE.std_logic_1164.all;
use work.p_MI0.all;

entity pcsrc is
	port 	(
			instr: in std_logic_vector(31 downto 0);
			pcsrc_out: out std_logic_vector(1 downto 0);
			branch: in std_logic;
			igual: in std_logic;
			jal: in std_logic;
			EX_instr: in std_logic_vector(31 downto 0);
			ID_instr: in std_logic_vector(31 downto 0)	
		);
end pcsrc;


architecture arq_pcsrc of pcsrc is


begin
   
    process (instr)
    begin
 	if (ID_instr(31 downto 26) = "000000" and ID_instr(5 downto 0) = "001000") then -- jr
		pcsrc_out <= "11";
	elsif   jal = '1' then -- jal
		pcsrc_out <= "01";
	elsif (ID_instr(31 downto 26) = "000100" and branch = '1' and igual = '1') then -- beq
		pcsrc_out <= "10";
	else 	-- outros
		pcsrc_out <= "00";
	end if;	  
     end process;

end arq_pcsrc;
