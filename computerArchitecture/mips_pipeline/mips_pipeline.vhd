library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_bit.all;
use work.p_MI0.all;

entity mips_pipeline is
	port (
		clk: in std_logic;
		reset: in std_logic
	);
end mips_pipeline;


architecture arq_mips_pipeline of mips_pipeline is
   

    -- ********************************************************************
    --                              Signal Declarations
    -- ********************************************************************
     
    -- IF Signal Declarations
    signal IF_PCSrc: std_logic_vector(1 downto 0);
    signal IF_instr, IF_pc, IF_pc_next, IF_pc_really_next, IF_Jsrc,IF_MUX2, IF_pc4 : reg32 := (others => '0');
    signal flush: std_logic;

    -- ID Signal Declarations

    signal ID_instr,ID_jr, ID_pc4, ID_PC, ID_btgt,ID_offset,ID_SaidaMuxA,ID_SaidaMuxB :reg32;  -- pipeline register values from EX
    signal ID_op, ID_funct: std_logic_vector(5 downto 0);
    signal ID_rs, ID_rt, ID_rd: std_logic_vector(4 downto 0);
    signal ID_immed: std_logic_vector(15 downto 0);
    signal ID_extend, ID_A, ID_B : reg32;
    signal ID_RegWrite, ID_Branch, ID_RegDst, ID_MemtoReg, ID_MemRead, ID_MemWrite, ID_ALUSrcB: std_logic; --ID Control Signals
    signal ID_ALUOp,ID_forward_A,ID_forward_B: std_logic_vector(1 downto 0);
    signal ID_Jal: std_logic;
    signal stall, igual: std_logic;

    -- EX Signals

    signal EX_pc4,EX_jr,EX_extend, EX_A, EX_B: reg32;
    signal EX_offset, EX_btgt, EX_alub, EX_ALUOut, EX_instr, SaidaMUX_A, SaidaMUX_B: reg32;
    signal EX_rt, EX_rd, EX_rs, EX_SaidaMux: std_logic_vector(4 downto 0);
    signal EX_RegRd: std_logic_vector(4 downto 0);
    signal EX_funct: std_logic_vector(5 downto 0);
    signal EX_RegWrite, EX_Branch, EX_RegDst, EX_MemtoReg, EX_MemRead, EX_MemWrite, EX_ALUSrcB: std_logic;  -- EX Control Signals
    signal EX_Zero: std_logic;
    signal EX_ALUOp: std_logic_vector(1 downto 0);
    signal EX_Operation: std_logic_vector(2 downto 0);
    signal EX_Jal: std_logic;
    signal EX_forward_A, EX_forward_B: std_logic_vector(1 downto 0);
 
    

   -- MEM Signals

    signal MEM_PCSrc: std_logic_vector(1 downto 0);
    signal MEM_RegWrite, MEM_Branch, MEM_MemtoReg, MEM_MemRead, MEM_MemWrite, MEM_Zero: std_logic;
    signal MEM_btgt, MEM_ALUOut, MEM_B, MEM_pc4: reg32;
    signal MEM_memout: reg32;
    signal MEM_RegRd: std_logic_vector(4 downto 0);
    signal MEM_Jal: std_logic;

    -- WB Signals

    signal WB_RegWrite, WB_MemtoReg: std_logic;  -- WB Control Signals
    signal WB_memout, WB_ALUOut, WB_pc4: reg32;
    signal WB_wd, Saida_MUX: reg32;
    signal WB_RegRd: std_logic_vector(4 downto 0);
    signal WB_Jal: std_logic;	
    	


begin -- BEGIN MIPS_PIPELINE ARCHITECTURE

    -- ********************************************************************
    --                              IF Stage
    -- ********************************************************************

    -- IF Hardware


    CTRLPC: entity work.pcsrc port map(IF_instr, IF_PCSrc,ID_Branch, igual, ID_Jal,EX_instr,ID_instr); -- Sinal de controle para o mux do PC.	

    PC4: entity work.add32 port map (IF_pc, x"00000004", IF_pc4); 

    MX2: entity work.mux2 port map (stall, IF_pc_next, IF_pc, IF_pc_really_next); -- Caso ocorra um stall o PC se mantém. 

    PC: entity work.reg port map (clk, reset, IF_pc_really_next, IF_pc); -- Seta o próximo PC.

    IF_Jsrc <= ID_pc(31 downto 28) & ID_instr(25 downto 0) & b"00"; -- Endereço do desvio do JAL.

    MX4: entity work.mux4 port map (IF_PCSrc, IF_pc4, IF_Jsrc, ID_btgt, ID_jr, IF_pc_next); -- MUX que vai para o PC.	

    ROM_INST: entity work.rom32 port map (IF_pc, IF_instr);
     
     
    flush <= '1' when (ID_instr(31 downto 26) = "000011") or --JAL.
    		      (ID_instr(31 downto 26) = "000000" and ID_instr(5 downto 0) = "001000") or -- JR no ID. 	
    		      ((ID_instr(31 downto 26) = "000100") and igual = '1') --BEQ.
    		       else
    	     '0';       	



    IF_s: process(clk)
    begin     			-- IF/ID Pipeline Register
			if rising_edge(clk) then
			  	if     reset = '1' then

					ID_instr <= (others => '0');
		    			ID_pc4   <= (others => '0');

				elsif (flush = '1') then

		    			ID_instr <= (others => '0');
					ID_pc4   <= IF_pc4;

				elsif (stall = '1') then
					-- nop
				else
					ID_instr <= IF_instr;
		    			ID_pc4   <= IF_pc4;
					ID_PC	 <= IF_PC;
			end if;
	end if;

    end process;



    -- ********************************************************************
    --                              ID Stage
    -- ********************************************************************

    ID_op <= ID_instr(31 downto 26);
    ID_rs <= ID_instr(25 downto 21);
    ID_rt <= ID_instr(20 downto 16);
    ID_rd <= ID_instr(15 downto 11);
    ID_immed <= ID_instr(15 downto 0);


    REG_FILE: entity work.reg_bank port map ( clk, reset, WB_RegWrite, ID_rs, ID_rt, WB_RegRd, ID_A, ID_B, WB_wd);

    UDH: entity work.UDH port map (EX_MemRead, EX_rt, ID_rs, ID_rt, WB_RegRd, stall); -- Unidade de Detecção de Hazard.
    
    beq_forward_A: entity work.forwardA port map (EX_RegWrite, MEM_RegWrite, MEM_RegRd, EX_RegRd, ID_rs, ID_forward_A); -- Unidade Forward A.

    beq_forward_B: entity work.forwardA port map (EX_RegWrite, MEM_RegWrite, MEM_RegRd, EX_RegRd, ID_rt, ID_forward_B); -- Unidade Forward B.

    MUX_a_beq: entity work.mux3 port map (ID_Forward_A, ID_A, EX_ALUOut, MEM_ALUOut, ID_SaidaMuxA); -- MUX A para fazer o adiantamento para o BEQ

    MUX_b_beq: entity work.mux3 port map (ID_Forward_B, ID_B, EX_ALUOut, MEM_ALUOut, ID_SaidaMuxB); -- MUX B para fazer o adiantamento para o BEQ

    SIGN_EXT: entity work.shift_left port map (ID_extend, 2, ID_offset); -- Deslocamento de 2 movido para o estágio ID.

    BRANCH_ADD: entity work.add32 port map (ID_pc4, ID_offset, ID_btgt); -- Endereço do Branch movido para o estágio ID.


	-------------------------------------------------------------------
    compara: process(ID_SaidaMuxA,ID_SaidaMuxB)	
    begin
	
		if ID_SaidaMuxA = ID_SaidaMuxB then 
			igual <= '1';
		else 
			igual <= '0';
	
		end if;
		
    end process;		


	-------------------------------------------------------------------



    -- sign-extender
    EXT: process(ID_immed)
    begin
	if ID_immed(15) = '1' then
		ID_extend <= x"FFFF" & ID_immed(15 downto 0);
	else
		ID_extend <= x"0000" & ID_immed(15 downto 0);
	end if;
    end process;
    

    CTRL: entity work.control_pipeline port map (ID_op, ID_RegDst, ID_ALUSrcB, ID_MemtoReg, ID_RegWrite, ID_MemRead, ID_MemWrite, ID_Branch, ID_ALUOp, ID_Jal);
    ID_jr <=  ID_A; -- JR recebe o endereço para desvio.

    ID_EX_pip: process(clk)		    -- ID/EX Pipeline Register
    begin
	if rising_edge(clk) then
        	if reset = '1' or stall = '1' then
            			EX_RegDst   <= '0';
	    			EX_ALUOp    <= (others => '0');
            			EX_ALUSrcB  <= '0';
		    		EX_Branch   <= '0';
				EX_MemRead  <= '0';
				EX_MemWrite <= '0';
				EX_RegWrite <= '0';
				EX_MemtoReg <= '0';
				EX_Jal      <= '0';
				
				EX_instr    <= (others => '0'); 
				EX_pc4      <= (others => '0');
				EX_A        <= (others => '0');
				EX_B        <= (others => '0');
				EX_extend   <= (others => '0');
				EX_rt       <= (others => '0');
				EX_rd       <= (others => '0');
							
        	else 
        		EX_RegDst   <= ID_RegDst;
        		EX_ALUOp    <= ID_ALUOp;
        		EX_ALUSrcB  <= ID_ALUSrcB;
        		EX_Branch   <= ID_Branch;
        		EX_MemRead  <= ID_MemRead;
        		EX_MemWrite <= ID_MemWrite;
        		EX_RegWrite <= ID_RegWrite;
        		EX_MemtoReg <= ID_MemtoReg;
      
        		EX_instr    <= ID_instr;
			EX_Jal      <= ID_Jal;
			EX_pc4      <= ID_pc4;
        		EX_A        <= ID_A;
        		EX_B        <= ID_B;
        		EX_extend   <= ID_extend;
        		EX_rt       <= ID_rt;
        		EX_rd       <= ID_rd;
        		EX_rs	    <= ID_rs;	   	
        	end if;
	end if;
    end process;

    -- ********************************************************************
    --                              EX Stage
    -- ********************************************************************

    -- branch offset shifter
    --SIGN_EXT: entity work.shift_left port map (EX_extend, 2, EX_offset);

    EX_funct <= EX_extend(5 downto 0);  
            
    MUX_A: entity work.mux3 port map (EX_forward_A, EX_A, MEM_ALUOut, WB_wd, SaidaMUX_A); -- MUX da entrada A da ULA.
    
    MUX_B: entity work.mux3 port map (EX_forward_B, EX_B , MEM_ALUOut, WB_wd, SaidaMUX_B); -- MUX para o MUX da entrada B da ULA.

    ALU_MUX_A: entity work.mux2 port map (EX_ALUSrcB, SaidaMUX_B, EX_extend, EX_alub); -- MUX da entrada B da ULA.

    ALU_h: entity work.alu port map (EX_Operation, SaidaMUX_A, EX_alub, EX_ALUOut, EX_Zero); -- Entradas modificadas
    
    forward_A: entity work.forwardA port map (MEM_RegWrite, WB_RegWrite, WB_RegRd, MEM_RegRd, EX_Rs, EX_forward_A); -- Unidade Forward A.

    forward_B: entity work.forwardA port map (MEM_RegWrite, WB_RegWrite, WB_RegRd, MEM_RegRd, EX_rt, EX_forward_B); -- Unidade Forward B.

    DEST_MUX2: entity work.mux2 generic map (5) port map (EX_RegDst, EX_rt, EX_rd, EX_SaidaMux);

    DEST_REG: entity work.mux2 generic map (5) port map (EX_Jal,EX_SaidaMux, b"11111",EX_RegRd); -- Endereço do registrador que será escrito.	

    ALU_c: entity work.alu_ctl port map (EX_ALUOp, EX_funct, EX_Operation);
    
    

    EX_MEM_pip: process (clk)		    -- EX/MEM Pipeline Register
    begin
	if rising_edge(clk) then
        	if reset = '1' then
            		MEM_Branch   <= '0';
            		MEM_MemRead  <= '0';
            		MEM_MemWrite <= '0';
            		MEM_RegWrite <= '0';
            		MEM_MemtoReg <= '0';
            		MEM_Zero     <= '0';
			MEM_Jal 	 <= '0';
			MEM_pc4      <= (others => '0');
            		MEM_btgt     <= (others => '0');
            		MEM_ALUOut   <= (others => '0');
            		MEM_B        <= (others => '0');
            		MEM_RegRd    <= (others => '0');
					
        	else
            		MEM_Branch   <= EX_Branch;
            		MEM_MemRead  <= EX_MemRead;
            		MEM_MemWrite <= EX_MemWrite;
            		MEM_RegWrite <= EX_RegWrite;
            		MEM_MemtoReg <= EX_MemtoReg;
            		MEM_Zero     <= EX_Zero;
			MEM_pc4      <= EX_pc4;
			MEM_Jal      <= EX_Jal; 
            		MEM_btgt     <= EX_btgt;
            		MEM_ALUOut   <= EX_ALUOut;
               		MEM_B <= SaidaMUX_B; -- Faz o adiantamento do SW no estágio de MEM.
            		MEM_RegRd    <= EX_RegRd;
        	end if;
	end if;
    end process;

    -- ********************************************************************
    --                              MEM Stage
    -- ********************************************************************

    MEM_ACCESS: entity work.mem32 port map (clk, MEM_MemRead, MEM_MemWrite, MEM_ALUOut, MEM_B, MEM_memout);


    MEM_WB_pip: process (clk)		-- MEM/WB Pipeline Register
    begin
	if rising_edge(clk) then
	        if reset = '1' then
            		WB_RegWrite <= '0';
            		WB_MemtoReg <= '0';
            		WB_ALUOut   <= (others => '0');
            		WB_memout   <= (others => '0');
            		WB_RegRd    <= (others => '0');
			WB_pc4      <=  (others => '0');
			WB_Jal <= '0';
			
        	else
            		WB_RegWrite <= MEM_RegWrite;
            		WB_MemtoReg <= MEM_MemtoReg;
            		WB_ALUOut   <= MEM_ALUOut;
            		WB_memout   <= MEM_memout;
            		WB_RegRd    <= MEM_RegRd;
			WB_pc4 <= MEM_pc4;
			WB_Jal <= MEM_Jal;
	

        	end if;
	end if;
    end process;       

    -- ********************************************************************
    --                              WB Stage
    -- ********************************************************************

    MUX_DEST: entity work.mux2 port map (WB_MemtoReg, WB_ALUOut, WB_memout, Saida_MUX);

    MUX_WB: entity work.mux2 port map(WB_Jal, Saida_MUX, WB_pc4, WB_wd); -- Se for JAL, escreve o PC+4 no R31.
	
    
    --REG_FILE: reg_bank port map (clk, reset, WB_RegWrite, ID_rs, ID_rt, WB_RegRd, ID_A, ID_B, WB_wd); *instance is the same of that in the ID stage


end arq_mips_pipeline;

