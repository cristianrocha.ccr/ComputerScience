#include <stdio.h>
#include <stdlib.h>
#include <string.h>



#include "ciclico.h"
#include "io.h"
#include "lista_adj.h"
#include "conexa.h"







void inicia_vetor(int marcado[1000]){
    int i;
    for(i = 0; i <= tamVertice; i++){
        
        marcado[i] = 0;
    }
       
}


void inicia_matriz(int explora[1000][1000]){
    int i, j;
    for(i = 0; i <= tamVertice +1; i++){       
        for(j = 0; j <= tamVertice+1; j++)
            explora[i][j] = 0;
    }
       
}


int dfs(TipoVizinho *vizinho, int u, int marcado[1000], int explorado[1000][1000]){
    
    ApontadorVizinho aux;
    int i, retorno = 0, achou = 0;
    aux = vizinho->Primeiro;
    
    while((aux->Prox!=NULL)&&!achou){
        aux = aux->Prox;
        if (aux->u == u)
                achou = 1;
        
        
    }
    
    if (aux == vizinho->Primeiro)
        aux = aux->Prox;
        
        
        for(i=0;i < aux->qtdVizinhos; i++){
            if(((explorado[aux->u][aux->vizinhos[i]]) == 0) || ((explorado[aux->vizinhos[i]][aux->u]) == 0)){
                explorado[aux->u][aux->vizinhos[i]] = 1;
                explorado[aux->vizinhos[i]][aux->u] =1;
                if((marcado[aux->vizinhos[i]])!=0)
                    return 1;
                else{
                    marcado[aux->vizinhos[i]] = 1;
                    retorno = dfs(vizinho,aux->vizinhos[i], marcado, explorado);
                }
                    
            }
            
            
        }
 
    
    
    return retorno;
    
    
    
    
}



int PrimeiroVizinho(TipoLista *Lista){
	ApontadorLista aux;
	
	aux = Lista->Primeiro;
        return aux->Prox->u;

}

int ProximoVizinho(ApontadorVizinho conexo){

	return conexo->vizinhos[0];
}


int main(int argc, char *argv[]){
	int  primvizinho, ciclico, vertices[1000], marcado[1000], explorado[1000][1000];
	TipoLista *aresta; 
	TipoVizinho *vizinho, *conexo;

	aresta = malloc(sizeof(CelulaLista));
	scanf("%d", &tamVertice);
	scanf("%d", &tamAresta);




	InsereArestas(tamAresta, aresta);
	vizinho = malloc(sizeof(CelulaVizinho));
	PegaVertices(aresta,vertices,vizinho);
	conexo = malloc(sizeof(CelulaVizinho));
	Acha_conexo(conexo, vizinho);

	ApontadorVizinho aux;

	aux = conexo->Primeiro;

	while(aux->Prox!=NULL){
		aux = aux->Prox;
		inicia_vetor(marcado);
	   inicia_matriz(explorado);
	   marcado[ProximoVizinho(aux)] = 1;
	   ciclico = dfs(vizinho, ProximoVizinho(aux), marcado, explorado);
		if( ciclico == 1)
			break;
		   
	}
	
	printf("%d \n", ciclico);

}
