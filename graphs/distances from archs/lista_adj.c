#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "grafos.h"
#include "lista_adj.h"
#include "conexa.h"
#include "io.h"

int Compara(int u, TipoVizinho *vizinho, int v){

	ApontadorVizinho aux;
	
	int teste=0;
	aux = vizinho->Primeiro;

	while(aux->Prox!=NULL){
		aux= aux->Prox;
		if((aux->u) == u){
			aux->vizinhos[aux->qtdVizinhos] = v;
			
			aux->qtdVizinhos = aux->qtdVizinhos + 1;
			return 0;	
		}
		teste++;
		
	
	}
	

	
	return 1;
}

	

void OrdenaVizinho(TipoVizinho *vizinho){
    
    ApontadorVizinho aux;
    int i,num,vet[30];
    aux = vizinho->Primeiro;
       
    
    while(aux->Prox!=NULL){
        i = 0;
        aux=aux->Prox;
         while(i < aux->qtdVizinhos){
            vet[i] = aux->vizinhos[i];
            i++;           
        }
        i = aux->qtdVizinhos - 1;
        while(i >= 0){
                num = AchaMaior(vet, (aux->qtdVizinhos - 1)); 
                aux->vizinhos[i] = num;
                i--;
        }
        
   }
}
void Complementa(TipoLista *aresta, TipoVizinho *vizinho){
	ApontadorVizinho aux1;
	ApontadorLista aux2;

	aux1 = vizinho->Primeiro;

	
	while(aux1->Prox!=NULL){
		aux1 = aux1->Prox;
		aux2 = aresta->Primeiro;
		printf(" \n");
		while(aux2->Prox!=NULL){
			
			aux2 = aux2->Prox;
			//printf("aux1->u %d aux2->v %d\n", aux1->u, aux2->v);			
			if(aux1->u == aux2->v){
				aux1->vizinhos[aux1->qtdVizinhos] = aux2->u;
				aux1->qtdVizinhos++;
			}

		}
		
	}
        aux1 = vizinho->Primeiro;
        
        while(aux1->Prox!=NULL){
            aux1 = aux1->Prox;
            aux2 = aresta->Primeiro;
            while(aux2->Prox!=NULL){
			
			aux2 = aux2->Prox;
	
		}
        }
                
        


	


}


int NaoPertence( ApontadorVizinho aux,int v){
    int i;
    for(i = 0; i < aux->qtdVizinhos; i++){
               if(aux->vizinhos[i] == v)
            return 0;
    }
    
    return 1;
    
    
}


int Verifica_Complementa(int u, TipoVizinho *vizinho, int v){

	ApontadorVizinho aux;
	
	int teste=0;
	aux = vizinho->Primeiro;

	while(aux->Prox!=NULL){
		aux= aux->Prox;
		if(((aux->u) == u)&&(NaoPertence(aux, v))){
			aux->vizinhos[aux->qtdVizinhos] = v;
			
			aux->qtdVizinhos = aux->qtdVizinhos + 1;
			return 0;	
		}
                else if(((aux->u) == u)&&(!NaoPertence(aux, v)))
                    return 0;
		teste++;
		
	
	}
	

	
	return 1;
}


void PegaVertices(TipoLista *aresta, int vertices[tamVertice], TipoVizinho *vizinho){
	ApontadorLista aux, aux2;
	int verdade;
	aux = aresta->Primeiro;
	VizinhoVazio(vizinho);
	while(aux->Prox!=NULL){
		
		aux = aux->Prox;
		verdade = Compara(aux->u, vizinho, aux->v);
		if((verdade) && (aux->u != 0)){
				
			vizinho->Ultimo->Prox = malloc(sizeof(CelulaVizinho));
		
			if (vizinho->Ultimo->Prox == NULL)
			{
       				printf ("Erro na alocação\n");
			}
			vizinho->Ultimo = vizinho->Ultimo->Prox;
			vizinho->Ultimo->Prox = NULL;	
			vizinho->Ultimo->u = aux->u;
			vizinho->Ultimo->vizinhos[0] = aux->v;
			vizinho->Ultimo->qtdVizinhos =1;
			vizinho->Ultimo->marcado = 0;		
		}	
	}
	
		
	
        Complementa(aresta, vizinho);
        
        aux = aresta->Primeiro;
        while(aux->Prox!=NULL){
            aux = aux->Prox;
            verdade = Verifica_Complementa(aux->v, vizinho, aux->u);
		if((verdade) && (aux->u != 0)){
				
			vizinho->Ultimo->Prox = malloc(sizeof(CelulaVizinho));
		
			if (vizinho->Ultimo->Prox == NULL)
			{
       				printf ("Erro na alocação\n");
			}
			vizinho->Ultimo = vizinho->Ultimo->Prox;
			vizinho->Ultimo->Prox = NULL;	
			vizinho->Ultimo->u = aux->v;
			vizinho->Ultimo->vizinhos[0] = aux->u;
			vizinho->Ultimo->qtdVizinhos =1;
			vizinho->Ultimo->marcado = 0;		
		}	
            
           
            
        }
              
        OrdenaVizinho(vizinho);       

}
