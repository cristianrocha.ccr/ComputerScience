
//Lista de arestas
typedef struct CelulaLista *ApontadorLista;

typedef struct CelulaLista
{
	int u,v;
	ApontadorLista Prox;		
}CelulaLista;



typedef struct 
{
	ApontadorLista Primeiro,Ultimo;
	
}TipoLista;


//Lista de adjacência
typedef struct CelulaVizinho *ApontadorVizinho;


typedef struct CelulaVizinho
{
	int u, marcado, qtdVizinhos, vizinhos[30];
	ApontadorVizinho Prox;
	
}CelulaVizinho;


typedef struct
{
	ApontadorVizinho Primeiro,Ultimo;

}TipoVizinho;



//Fila

typedef struct TipoCelulaFila *TipoApontadorFila;

typedef struct TipoCelulaFila
{
	int vertice;
	TipoApontadorFila Prox;		

}TipoCelulaFila;

typedef struct 
{
	TipoApontadorFila Frente, Tras;
}TipoFila;



TipoFila *f;


//----------------Variaveis Globais---------------------------------------------------------------------------------//

int tamVertice, tamAresta;

//------------------------------------------------------------------------------------------------------------------//

int PrimeiroVizinho(TipoLista *Lista);
