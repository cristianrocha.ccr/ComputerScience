#include <stdio.h>
#include <stdlib.h>
#include <string.h>



#include "grafos.h"
#include "conexa.h"
#include "io.h"
#include "lista_adj.h"

//Verifica se algum número pertence ao vetor da lista Vizinho e "lista->u"
int Pertence_todo(int num, ApontadorVizinho vizinho){
	

       while(vizinho->Prox!=NULL){ 
                vizinho = vizinho->Prox;
                if(vizinho->u == num)
                        return 1; 
        
                int i=0;
                while(i < vizinho->qtdVizinhos){
                        if(num == vizinho->vizinhos[i])
                                return 1;
                        i++;	
                }	
	
		
	
        }
       
       return 0;
}	


//Verifica se algum número pertence ao vetor da lista Vizinho
int Pertence(int num, ApontadorVizinho vizinho){
	int i=0;

        
       if(vizinho->u == num)
               return 1; 
        
        
        
	while(i < vizinho->qtdVizinhos){
		if(num == vizinho->vizinhos[i])
                	return 1;
		i++;	
	}	
	
		return 0;
	
	
}	

//Função recursiva que procura todas os vertices de uma lista Vizinho
void Procura(int num, ApontadorVizinho aux2, ApontadorVizinho aux, int cont, TipoVizinho *vizinho){

	if((cont+1)>aux2->qtdVizinhos)
		return;

	if(Pertence(num, aux) && !(aux->marcado)){
		JuntaVizinhos(aux2, aux);
		aux->marcado =1;
	}
	
	if (aux->Prox == NULL)
			Procura(aux2->vizinhos[cont +1], aux2, vizinho->Primeiro, (cont+1), vizinho);
        else                
	Procura(num, aux2, aux->Prox, cont, vizinho);
			
	
}



void Maloca(TipoVizinho *conexa){
	conexa->Ultimo->Prox = malloc(sizeof(CelulaVizinho));
	if (conexa->Ultimo->Prox == NULL)
	{
       		printf ("Erro na alocação\n");
	}
	
	conexa->Ultimo = conexa->Ultimo->Prox;
	conexa->Ultimo->qtdVizinhos = 0;
	conexa->Ultimo->Prox = NULL;
	
}	


//Junta os vizinhos na lista de conexividade
void JuntaVizinhos(ApontadorVizinho conexa,ApontadorVizinho vizinho){
	int i=0, j=0,verdade = 0;
	
	if(conexa->qtdVizinhos ==0)
                conexa->u = vizinho->u;
        
        else{
        
                while(i < conexa->qtdVizinhos){
                        if((vizinho->u == conexa->vizinhos[i]) || (conexa->u == vizinho->u))
                                verdade = 1;
                         i++;               
                }
                        if(!verdade){
                        conexa->vizinhos[conexa->qtdVizinhos] = vizinho->u;
                        conexa->qtdVizinhos++;
                
                }   
              i = 0;
        }    
        
       
	while(i < vizinho->qtdVizinhos){
		
		verdade = 0;	
                j = 0;
		while(j < conexa->qtdVizinhos){
			if(conexa->qtdVizinhos != 0){
			if((conexa->vizinhos[j] == vizinho->vizinhos[i]) || (vizinho->vizinhos[i] == conexa->u))
				verdade =1;
                        }	
			j++;
		}
			
		if(!verdade){
			conexa->vizinhos[conexa->qtdVizinhos] = vizinho->vizinhos[i];
			conexa->qtdVizinhos++;
		
		}
                i++;    
        }
	
		
	
}
	
		
int AchaMaior(int vet[30], int tam){
    int maior,i,pos;
    
    
    maior = vet[0];
    for(i=0;i<=tam;i++){
        if((maior < vet[i])&& (vet[i]!=(-1)) ){
            maior = vet[i];
            pos = i;
        }    
    }
    if(maior == vet[0])
        vet[0] = -1;
    else
        vet[pos] = -1;
    return maior;   
}	


//Ordena o vetor da lista de conexividade
int Ordena(TipoVizinho *conexa){
    
    ApontadorVizinho aux;
    int i,num,j,vet[30];
    aux = conexa->Primeiro;
    
    j = 0;
    
    
    
    while(aux->Prox!=NULL){
        i = 1;
			j++;
        aux=aux->Prox;
        vet[0] = aux->u;
        while(i <= aux->qtdVizinhos){
            vet[i] = aux->vizinhos[i-1];
            i++;           
        }
        i = aux->qtdVizinhos;
        while(i >= 0){
                num = AchaMaior(vet, aux->qtdVizinhos); 
                aux->vizinhos[i] = num;
                i--;
        }
        aux->u = 0;
   }
   
	return j; 
  
}




int Acha_conexo(TipoVizinho *conexa, TipoVizinho *vizinho){
	ApontadorVizinho aux1,aux2;
	int comeco=1, cont= -1, j;

	aux1 = vizinho->Primeiro;
	VizinhoVazio(conexa);
	aux2 = conexa->Primeiro;
	while(aux1->Prox!=NULL){
		aux1 = aux1->Prox;
		if(!Pertence_todo(aux1->u, conexa->Primeiro) || comeco){
			comeco = 0;
			Maloca(conexa);	
			aux2 = conexa->Ultimo;
			
			JuntaVizinhos(aux2, aux1);
			Procura(aux2->u, aux2, aux1, cont, vizinho);
		}	
	}
        
       
       j = Ordena(conexa);
		 //Número de Componentes conexas
		return j;
}
