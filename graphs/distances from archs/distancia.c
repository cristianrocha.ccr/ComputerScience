#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "distancia.h"
#include "io.h"
#include "lista_adj.h"



void inicia_vetor(int marcado[1000]){
    int i;
    for(i = 0; i <= tamVertice; i++){
        
        marcado[i] = 0;
    }
       
}


void inicia_matriz(int explora[1000][1000]){
    int i, j;
    for(i = 0; i <= tamVertice +1; i++){       
        for(j = 0; j <= tamVertice+1; j++)
            explora[i][j] = 0;
    }
       
}

void FilaVazia(TipoFila *Fila)
{
	Fila->Frente=(TipoApontadorFila)malloc(sizeof(TipoCelulaFila));
	Fila->Tras=Fila->Frente;
	Fila->Frente->Prox=NULL;
}


int VaziaFila(TipoFila *Fila)
{
	return (Fila->Frente == Fila->Tras);
}


void Enfileira (int x, TipoFila *Fila)
{

    Fila->Tras->Prox = (TipoApontadorFila)malloc(sizeof(TipoCelulaFila));
    if (Fila->Tras->Prox == NULL)
    {
        printf ("Erro na alocação de trem\n");
        return;      
    }
    Fila->Tras = Fila->Tras->Prox;
    Fila->Tras->Prox = NULL;
    Fila->Tras->vertice = x;
    
}


void Desenfileira(TipoFila *Fila)
{ 
    TipoApontadorFila q;
        
    if(VaziaFila(Fila))
    {
        printf("ERRO, Não é possível desenfileirar");
        return;
    }
    q = Fila->Frente;
    Fila->Frente=Fila->Frente->Prox;

    free(q);
}


void bfs(TipoVizinho *vizinho, int marcado[1000], int explorado[1000][1000], int nivel[1000][1000], int k){
    int u;
    ApontadorVizinho aux;
    int i, achou = 0;
    
    //Desenfileira(f);
    
    while(!VaziaFila(f)){
        achou = 0;    
        aux = vizinho->Primeiro;    
        u = f->Frente->Prox->vertice;
    
        while((aux->Prox!=NULL)&&!achou){
                aux = aux->Prox;
                if (aux->u == u)
                        achou = 1;
        }
    
        if (aux == vizinho->Primeiro)
                aux = aux->Prox;
        
        
        for(i=0;i < aux->qtdVizinhos; i++){
            if(((explorado[aux->u][aux->vizinhos[i]]) == 0) || ((explorado[aux->vizinhos[i]][aux->u]) == 0)){
                explorado[aux->u][aux->vizinhos[i]] = 1;
                explorado[aux->vizinhos[i]][aux->u] =1;
                if((marcado[aux->vizinhos[i]])!=0);
                    //aresta de cruzamento
                else{
                    marcado[aux->vizinhos[i]] = 1;
                    nivel[k][aux->vizinhos[i]] = nivel[k][aux->u] + 1;
                    Enfileira((int)(aux->vizinhos[i]),f);
                }
                    
            }
            
            
        }
 
    
    Desenfileira(f);
    
    
    }
}

int main(){


	int  primvizinho, ciclico, vertices[1000], marcado[1000], explorado[1000][1000], nivel[1000][1000];
	TipoLista *aresta; 
	TipoVizinho *vizinho;

	aresta = malloc(sizeof(CelulaLista));
	scanf("%d", &tamVertice);
	scanf("%d", &tamAresta);




	InsereArestas(tamAresta, aresta);
	vizinho = malloc(sizeof(CelulaVizinho));
	PegaVertices(aresta,vertices,vizinho);

	  f = malloc(sizeof(TipoCelulaFila));        
        ApontadorVizinho aux2;
        int k=1,j,i;
        aux2 = vizinho->Primeiro;
        inicia_matriz(nivel);
        while(aux2->Prox!=NULL){
                aux2 = aux2->Prox;
                FilaVazia(f);
                inicia_vetor(marcado);
                inicia_matriz(explorado);
              
                marcado[aux2->u] = 1;
                Enfileira(aux2->u, f);
                bfs(vizinho , marcado, explorado,nivel,aux2->u);
                k++;
        }        
                for(i = 1; i <= tamVertice; i++){
                        for(j = 1; j <=tamVertice; j++){
                            if(nivel[i][j] == 0 && i!= j)
                                printf("- ");
                            else
                            printf("%d ", nivel[i][j]);
                
                }
                        printf("\n");
        }
}
