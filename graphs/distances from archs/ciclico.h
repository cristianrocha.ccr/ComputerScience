/* 
 * File:   ciclo.h
 * Author: ccr
 *
 * Created on 12 de Dezembro de 2012, 00:03
 */

//Lista de arestas
typedef struct CelulaLista *ApontadorLista;

typedef struct CelulaLista
{
	int u,v;
	ApontadorLista Prox;		
}CelulaLista;



typedef struct 
{
	ApontadorLista Primeiro,Ultimo;
	
}TipoLista;


//Lista de adjacência
typedef struct CelulaVizinho *ApontadorVizinho;


typedef struct CelulaVizinho
{
	int u, marcado, qtdVizinhos, vizinhos[30];
	ApontadorVizinho Prox;
	
}CelulaVizinho;


typedef struct
{
	ApontadorVizinho Primeiro,Ultimo;

}TipoVizinho;


int tamVertice, tamAresta;

//------------------------------------------------------------------------------------------------------------------//

int PrimeiroVizinho(TipoLista *Lista);

int ProximoVizinho(ApontadorVizinho conexo);
void inicia_vetor(int marcado[1000]);
void inicia_matriz(int explora[1000][1000]);
int dfs(TipoVizinho *vizinho, int u, int marca[1000], int explora[1000][1000]);
