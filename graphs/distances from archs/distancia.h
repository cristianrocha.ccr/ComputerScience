/* 
 * File:   ciclo.h
 * Author: ccr
 *
 * Created on 12 de Dezembro de 2012, 00:03
 */

//Lista de arestas

typedef struct CelulaLista *ApontadorLista;

typedef struct CelulaLista
{
	int u,v;
	ApontadorLista Prox;		
}CelulaLista;



typedef struct 
{
	ApontadorLista Primeiro,Ultimo;
	
}TipoLista;


//Lista de adjacência
typedef struct CelulaVizinho *ApontadorVizinho;


typedef struct CelulaVizinho
{
	int u, marcado, qtdVizinhos, vizinhos[30];
	ApontadorVizinho Prox;
	
}CelulaVizinho;


typedef struct
{
	ApontadorVizinho Primeiro,Ultimo;

}TipoVizinho;


//Fila

typedef struct TipoCelulaFila *TipoApontadorFila;

typedef struct TipoCelulaFila
{
	int vertice;
	TipoApontadorFila Prox;		

}TipoCelulaFila;

typedef struct 
{
	TipoApontadorFila Frente, Tras;
}TipoFila;



TipoFila *f;


int tamVertice, tamAresta;

//------------------------------------------------------------------------------------------------------------------//

int PrimeiroVizinho(TipoLista *Lista);


void inicia_vetor(int marcado[1000]);
void inicia_matriz(int explora[1000][1000]);
void bfs(TipoVizinho *vizinho, int marcado[1000], int explorado[1000][1000], int nivel[1000][1000], int k);
void Desenfileira(TipoFila *Fila);
void Enfileira (int x, TipoFila *Fila);
int VaziaFila(TipoFila *Fila);
void FilaVazia(TipoFila *Fila);
