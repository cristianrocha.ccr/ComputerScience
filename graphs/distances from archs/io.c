#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "grafos.h"
#include "io.h"

void ImprimeAresta(TipoLista *lista){
	ApontadorLista aux;
	aux = lista->Primeiro;

	while(aux->Prox!=NULL){
		
		printf("%d %d\n", (aux)->Prox->u, (aux)->Prox->v);
		aux = (ApontadorLista)aux->Prox;
	}	
}



void ImprimeVizinhos(TipoVizinho *vizinho){
	ApontadorVizinho aux; int i;
	aux = vizinho->Primeiro;

	while(aux->Prox!=NULL){
            aux = (ApontadorVizinho)aux->Prox;
				
            for(i=0;i <= aux->qtdVizinhos;i++){
		printf("%d ", (aux)->vizinhos[i] );
            }
             printf("\n");
		
	}	
}


void ListaVazia(TipoLista *Lista)
{
	Lista->Primeiro = malloc(sizeof(CelulaLista));
	Lista->Ultimo = Lista->Primeiro;
	Lista->Primeiro->Prox=NULL;
}


void VizinhoVazio(TipoVizinho *Lista)
{
	Lista->Primeiro = malloc(sizeof(CelulaVizinho));
	Lista->Ultimo = Lista->Primeiro;
	Lista->Primeiro->Prox=NULL;
}




void InsereArestas(int tamAresta, TipoLista *aux)
{
	int i=0,verdade=1;
	while(i<tamAresta){
		if(verdade){
			ListaVazia(aux);
			verdade = 0;
				
		}
  		aux->Ultimo->Prox = malloc(sizeof(CelulaLista));
   		if (aux->Ultimo->Prox == NULL)
    		{
        		printf ("Erro na alocação\n");
    		}
    		aux->Ultimo = aux->Ultimo->Prox;
    		aux->Ultimo->Prox = NULL;
		scanf("%d %d", &(aux)->Ultimo->u, &(aux)->Ultimo->v);
		i++;
	}	
}
