#!/bin/bash

# escolha o caminho do cross-compilador e bin-utils
# DInf
export PATH=$PATH:/home/soft/linux/mips/cross/bin/
# casa export PATH=$PATH:/opt/cross/bin

# 
set -x

if [ $# = 0 ] ; then
    echo -e "\t$0 teste.c\n\tproduz prog.txt para ser lido por MRstd_tb"
    exit 1
fi

inp=${1%%.c}

mips-gcc -O1 -S ${inp}.c -o ${inp}.s && \
  ./troca.sh ${inp}.s && \
  mips-as -O0 -EB -g2 -mips1 -o ${inp}.o ${inp}.s && \
  mips-as -O0 -EB -g2 -mips1 -o start.o start.s && \
  mips-ld -e _start -Ttext 0x10000000 -Tdata 0x20000000 \
      -o ${inp}.elf start.o ${inp}.o && \
  mips-objdump -z -D -EB --show-raw-insn --section .text --section .data \
      ${inp}.elf | \
  sed -e "/elf32/d" \
      -e "/>:/d" \
      -e "/[.]text/s:^\(.*\)$:.CODE:" \
      -e "/[.]data/s:^\(.*\)$:.DATA:" \
      -e "s/^\([0-9A-Fa-f]......[0-9A-Fa-f]\):[ 	]\([0-9A-Fa-f]......[0-9A-Fa-f]\)[ 	]\(.*\)$/[0x\1] 0x\2	\3/" \
      -e "/^$/d" >prog.txt

rm -f ${inp}.{elf,o} start.o
