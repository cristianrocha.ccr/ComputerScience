	.file	1 "reduz.c"
	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 1
	.text
	.align	2
	.globl	rSUM
	.set	nomips16
	.ent	rSUM
	.type	rSUM, @function
rSUM:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	
	move	$3,$0
	move	$2,$0
	li	$5,128			# 0x80
$L2:
	addu	$6,$4,$3
	lw	$6,0($6)
	addiu	$3,$3,4
	addu	$2,$2,$6
	bne	$3,$5,$L2

	nop
	j	$31

	.set	macro
	.set	reorder
	.end	rSUM
	.size	rSUM, .-rSUM
	.align	2
	.globl	rPRO
	.set	nomips16
	.ent	rPRO
	.type	rPRO, @function
rPRO:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	
	move	$3,$0
	li	$2,1			# 0x1
	li	$5,128			# 0x80
	addu	$6,$4,$3
$L9:
	lw	$6,0($6)
	nop
	mult	$2,$6
	mflo	$2
	addiu	$3,$3,4
	addu	$6,$4,$3
	bne	$3,$5,$L9

	nop
	j	$31

	.set	macro
	.set	reorder
	.end	rPRO
	.size	rPRO, .-rPRO
	.align	2
	.globl	main
	.set	nomips16
	.ent	main
	.type	main, @function
main:
	.frame	$sp,32,$31		# vars= 0, regs= 3/0, args= 16, gp= 0
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	
	addiu	$sp,$sp,-32
	sw	$31,28($sp)
	sw	$17,24($sp)
	sw	$16,20($sp)
	lui	$16,%hi(dat)
	addiu	$4,$16,%lo(dat)
	jal	rSUM

	li	$17,536870912			# 0x20000000
	ori	$3,$17,0x1550
	sw	$2,0($3)
	addiu	$4,$16,%lo(dat)
	jal	rPRO

	ori	$17,$17,0x1ff0
	sw	$2,0($17)
	lw	$31,28($sp)
	lw	$17,24($sp)
	lw	$16,20($sp)
	addiu	$sp,$sp,32
	j	$31

	.set	macro
	.set	reorder
	.end	main
	.size	main, .-main
	.globl	dat
	.data
	.align	2
	.type	dat, @object
	.size	dat, 128
dat:
	.word	285216784
	.word	570425378
	.word	-11141291
	.word	16843009
	.word	285216784
	.word	570425378
	.word	-11141291
	.word	16843009
	.word	285216784
	.word	570425378
	.word	-11141291
	.word	16843009
	.word	285216784
	.word	570425378
	.word	-11141291
	.word	16843009
	.word	285216784
	.word	570425378
	.word	-11141291
	.word	16843009
	.word	285216784
	.word	570425378
	.word	-11141291
	.word	16843009
	.word	285216784
	.word	570425378
	.word	-11141291
	.word	16843009
	.word	-11141291
	.word	16843009
	.word	-11163000
	.word	16843009
	.ident	"GCC: (GNU) 4.4.3"
