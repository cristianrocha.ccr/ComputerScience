#!/bin/bash

# set -x

# se passar argumento para script, executa gtkwave
if [ $# = 1 ] ; then WAVE="sim"
else WAVE=
fi

ghdl --clean

ghdl -a --ieee=synopsys -fexplicit MRstd.vhd 2> log.txt
if [[ $? == 0 ]]; then
  ghdl -a --ieee=synopsys -fexplicit MRstd_tb.vhd 2>> log.txt
  if [[ $? == 0 ]]; then
    ghdl -e --ieee=synopsys -fexplicit MRstd_tb 2>> log.txt
    if [[ $? == 0 ]]; then
      ./mrstd_tb --stop-time=10000ns --vcd=mrstd.vcd 2>> log.txt
      # executa gtkwave sob demanda
      test -z $WAVE  ||  gtkwave mrstd.vcd g.sav
    else
      cat log.txt
    fi
  else
    cat log.txt
  fi
else
  cat log.txt
fi
