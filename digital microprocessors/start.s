	.text
	.align 2
	.globl _start
	.ent _start

_start:
	li $sp,0x20001ffc     # Start SP
	nop
	jal main
	nop
_exit:
	nop
	j _exit
	nop
	.end _start
	.align 2
