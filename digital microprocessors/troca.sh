cat $1 |\
  sed -e "/	bne/{
N
s:^\(.bne	 *[a-zA-Z0-9$,]*\)\n\(.*\)$:\2\n\1:
}
" |sed -e "/	beq/{
N
s:^\(.beq	 *[a-zA-Z0-9$,]*\)\n\(.*\)$:\2\n\1:
}
" |sed -e "/	jal/{
N
s:^\(.jal	 *[a-zA-Z0-9$%(),]*\)\n\(.*\)$:\2\n\1:
}
" |sed -e "/	jr/{
N
s:^\(.jr	 *[a-zA-Z0-9$%(),]*\)\n\(.*\)$:\2\n\1:
}
" |sed -e "/	j	/{
N
s:^\(.j	 *[a-zA-Z0-9$%(),]*\)\n\(.*\)$:\2\n\1:
}
" >x_x
mv x_x $1

# gcc gera codigo que preenche branch-delay-slots com instrucoes de
# antes do desvio/salto; este script retorna aa ordem original.
