#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "data.h"
#include "agturismo.h"
#include "disco.h"
#include "ihm.h"  
#include "cliente_chegada.h" 
#include "cliente_atendimento.h" 
#include "passeio.h" 
#include "hotel.h"   
#include "voo.h" 
#include "geracao_pacote.h"
#include "biblioteca.h"  



int main(){
			 
	int opcao,opcao2,permanece,num;
	
	char cpf[TAM_CPF+1];

	num=0;
		
	CarregaArquivos(&num);
	
	do
	{
		opcao = Menu();

		switch (opcao)
		{
			case 1: //1 Registrar chegada do cliente
				ChegadaCliente(&num);
				break;
			case 2:  //2 Atender cliente //ele inclui mesmo sem ter digitado o cpf
				if (num!=0)
				{
					num--;
					DesenfileiraCliente(&FilaCliente, cpf);	
					permanece=TRUE;
					do
					{
						if(BuscaCliente(cpf))
						{	
							opcao2 = MenuCliente();
							switch (opcao2)
							{
								case 1: 
									GerenciamentoClientes(cpf);
									break;
								case 2:
									GeracaoPacoteTuristico();
									break;
								case 3: //Retorna
									permanece=FALSE;
									break;
								default:
									printf(" Operador invalido");
							}
						}	
						else
						{
							IncluirCliente(cpf);
						}		
					
					}while (permanece==TRUE);
				}
				else
				{
					printf("\n Não há clientes na fila\n");
				}	
				break;
			case 3:  //3 Gerenciamento de hotéis
				GerenciamentoHoteis();
				break;
			case 4:	//4 Gerenciamento de vôos
				GerenciamentoVoos();
				break;
			case 5: //5 Gerenciamento de passeios
				GerenciamentoPasseios();
				break;
			case 6:  //6 Terminar Programa
				SalvaArquivos();
				break;
			default:
				printf(" Operador invalido");
				break;	
		}
	} while(opcao != 6);
	

}
