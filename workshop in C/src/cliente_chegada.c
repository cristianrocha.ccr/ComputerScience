#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "data.h"
#include "agturismo.h"
#include "ihm.h"   
#include "cliente_chegada.h" 



void FilaVaziaCliente(TipoFilaCliente *Fila)
{
	Fila->Frente=(TipoApontadorFilaCliente)malloc(sizeof(TipoCelulaFilaCliente));
	Fila->Tras=Fila->Frente;
	Fila->Frente->Prox=NULL;
}

int VaziaFilaCliente(TipoFilaCliente Fila)
{
	return (Fila.Frente == Fila.Tras);
}

int EnfileiraCliente (TipoItemFilaCliente x, TipoFilaCliente *Fila)
{
    Fila->Tras->Prox = (TipoApontadorFilaCliente)malloc(sizeof(TipoCelulaFilaCliente));
    if (Fila->Tras->Prox == NULL)
    {
        printf ("Erro na alocação de cliente\n");
        return erro=TRUE;        
    }
    Fila->Tras = Fila->Tras->Prox;
    Fila->Tras->Prox = NULL;
    Fila->Tras->Item = x;
    return erro=FALSE;    
}


void DesenfileiraCliente(TipoFilaCliente *Fila, char *cpf)
{ 
    TipoApontadorFilaCliente q;
    TipoItemFilaCliente Item;
    
    if(VaziaFilaCliente(*Fila))
    {
        printf("ERRO, Não é possível desenfileirar");
        return;
    }
    q = Fila->Frente;
    Fila->Frente=Fila->Frente->Prox;
    Item = Fila->Frente->Item;
    
    strcpy(cpf,Item.cpf);
    free(q);
}

void RegistraChegada(int *num)
{
	char cpf[TAM_CPF+1];
	TipoItemFilaCliente Cliente;
	
	LeCPF(Cliente.cpf);
	
    if(ProcuraFilaCliente(Cliente.cpf))
    {
		printf("Cliente já está na fila\n");
		return;
	}	
		
    erro=EnfileiraCliente(Cliente,&FilaCliente);
    if(erro==FALSE)
	{
		printf("\n Enfileirado\n");
		(*num)++;
    }
    else
		printf("\n Não Enfileirado\n");

    return;
}

int ProcuraFilaCliente(char *cpf)
{
    int encontrado;
    TipoApontadorFilaCliente Cliente;
       
    Cliente = FilaCliente.Frente->Prox;

    while((Cliente != NULL)&&( strcmp( Cliente->Item.cpf, cpf) ))
    {
		Cliente = Cliente->Prox;
	}
	
	if(Cliente != NULL)
	{
        return encontrado=TRUE;
	}
	else
	    return encontrado=FALSE;

}

void ChegadaCliente(int *num)
{
	int opcao,permanece;
	permanece=TRUE;
	do
	{
		opcao = MenuChegadaCliente();
		switch (opcao)
		{
	 		case 1:
				RegistraChegada(num);
				break;
			case 2: //Retorna
				permanece=FALSE;
				break;
			default:
				printf(" Operador invalido");
		}		
	}while (permanece==TRUE);	
	
}	
