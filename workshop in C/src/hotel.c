#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "data.h"
#include "agturismo.h"
#include "ihm.h"
#include "disco.h"


TipoApontadorHotel ProcuraHotel(char *nome,  TipoApontadorHotel *Hotel)
{
    TipoApontadorHotel HotelAnterior;
    
    HotelAnterior = ListaHotel.Primeiro;
    *Hotel = ListaHotel.Primeiro->Prox;

    while((*Hotel != NULL)&&(strcmp((*Hotel)->Item.nome, nome)!=0))
    {
       HotelAnterior = HotelAnterior->Prox;
       	*Hotel = (*Hotel)->Prox;
	}

	if(*Hotel != NULL)
	{
        return HotelAnterior;
	}
	else
	    return HotelAnterior=NULL;
}

void AlterarDadosHotel()
{
    TipoApontadorHotel Hotel;
    char nome[TAM_NOME_HOTEL+1];
    int num;
    printf("\nAlterar dados de hotel\n");
	
	LeNomeHotel(nome);
	    
    ProcuraHotel(nome, &Hotel);
    if(Hotel != NULL)
	{
		ImprimeHotel(Hotel);    		        
     
	 	  
	        printf("Altera Nome? ");
	        if(LeSimNao())
    	    	{
				LeNomeHotel(Hotel->Item.nome);
	        }
	        printf("Altera endereço? ");
	        if(LeSimNao())
	        {
	            LeEnderecoHotel(Hotel->Item.endereco);
	        }
	        printf("Altera cidade?  ");
	        if(LeSimNao())
	        {
	            LeCidade(Hotel->Item.cidade);
	        }
	        printf("Altera diaria? ");
	        if(LeSimNao())
	        {
	        	    LeDiaria(&Hotel->Item.diaria);
        	}
			printf("Altera quartos? ");
			if(LeSimNao())
				{
	        	    LeQuartos(&Hotel->Item.quartos);
				}
		
	}
	else
		printf("\nNão encontrado\n");

}

int InsereHotel (TipoItemHotel x, TipoListaHotel *Lista)
{
    Lista->Ultimo->Prox = (TipoApontadorHotel)malloc(sizeof(TipoCelulaHotel));
    if (Lista->Ultimo->Prox == NULL)
    {
        printf ("Erro na alocação de Hotel\n");
        return erro=TRUE;        
    }
    Lista->Ultimo = Lista->Ultimo->Prox;
    Lista->Ultimo->Prox = NULL;
    Lista->Ultimo->Item = x;
    return erro=FALSE;    
}

void IncluirHotel()
{
    TipoItemHotel Hotel;
    TipoApontadorHotel PontCelHotel;
    
    printf("Inclusão de Hotel\n");

	LeNomeHotel(Hotel.nome);
    ProcuraHotel(Hotel.nome, &PontCelHotel);
    if(PontCelHotel == NULL)
    {
		LeEnderecoHotel(Hotel.endereco);
		LeCidade(Hotel.cidade);
		LeDiaria(&Hotel.diaria);
		LeQuartos(&Hotel.quartos);
        erro=InsereHotel(Hotel,&ListaHotel);
        if(erro==FALSE)
            printf("\n Incluido\n");
        else
            printf("\n Não Incluido\n");
    }
    else
    {
        printf("\nHotel já está incluso\n");
    }
    return;
}

void RetiraHotel(TipoApontadorHotel p, TipoListaHotel *Lista, TipoItemHotel *Item)
{ // o item a ser retirado é o seguinte ao apontado por p
    TipoApontadorHotel q;
    if(VaziaHotel(*Lista) || p==NULL || p->Prox ==NULL )
    {
        printf("ERRO, Não é possível excluir Hotel\n");
        return;
    }
    q = p->Prox;
    *Item = q->Item;
    p->Prox = q->Prox;
    if (p->Prox == NULL)
    {
        Lista->Ultimo = p; 
    } 
    free(q);
}

void ExcluirHotel() 
{
    TipoApontadorHotel Anterior, Hotel;
    TipoItemHotel Item;
    int remove,num;
    char nome[TAM_NOME_HOTEL+1];
    printf("\nExcluir hotel\n");
	
	LeNomeHotel(nome);	
	    
    Anterior=ProcuraHotel(nome, &Hotel);
    if(Hotel != NULL)
	{
        ImprimeHotel(Hotel);
		printf("Remove? ");
	        if(LeSimNao()==1)
	        {
	            RetiraHotel(Anterior, &ListaHotel, &Item);
	            printf("Hotel com nome: %s removido\n",Item.nome);     
	        }
	}
	else
	   	printf("\nNão encontrado\n");
}

void GerenciamentoHoteis()
{
	int opcao,permanece;
	permanece=TRUE;
	do
	{
		opcao = MenuGerenciamentoHoteis();
		switch (opcao)
		{
	 		case 1:
				IncluirHotel();
				break;
			case 2:
				AlterarDadosHotel();
				break;
			case 3:
				ExcluirHotel();
				break;
			case 4: //Retorna
				permanece=FALSE;
				break;
			default:
				printf(" Operador invalido");
		}		
	}while (permanece==TRUE);	
}	
