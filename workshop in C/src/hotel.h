TipoApontadorHotel ProcuraHotel(char *nome,  TipoApontadorHotel *Hotel);
void AlterarDadosHotel();
int InsereHotel (TipoItemHotel x, TipoListaHotel *Lista);
void IncluirHotel();
void RetiraHotel(TipoApontadorHotel p, TipoListaHotel *Lista, TipoItemHotel *Item);
void ExcluirHotel();
