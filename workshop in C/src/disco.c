#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#include "data.h"
#include "agturismo.h"
#include "cliente_atendimento.h"
#include "hotel.h"
#include "voo.h"
#include "passeio.h"
#include "disco.h"


//LISTAS VAZIAS
void ListaVaziaCliente(TipoListaCliente *Lista)
{
	Lista->Primeiro=(TipoApontadorCliente)malloc(sizeof(TipoCelulaCliente));
	Lista->Ultimo=Lista->Primeiro;
	Lista->Primeiro->Prox=NULL;
}
void ListaVaziaHotel(TipoListaHotel *Lista)
{
	Lista->Primeiro=(TipoApontadorHotel)malloc(sizeof(TipoCelulaHotel));
	Lista->Ultimo=Lista->Primeiro;
	Lista->Primeiro->Prox=NULL;
}
void ListaVaziaVoo(TipoListaVoo *Lista)
{
	Lista->Primeiro=(TipoApontadorVoo)malloc(sizeof(TipoCelulaVoo));
	Lista->Ultimo=Lista->Primeiro;
	Lista->Primeiro->Prox=NULL;
}

void PilhaVaziaPasseio (TipoPilhaPasseio *Pilha)
{
	Pilha->Topo = (TipoApontadorPasseio)malloc (sizeof(TipoCelulaPasseio));
	Pilha->Fundo = Pilha->Topo;
	Pilha->Topo->Prox=NULL;
	Pilha->tamanho = 0;
}


int VaziaCliente(TipoListaCliente Lista)
{
	return (Lista.Primeiro == Lista.Ultimo);
}

int VaziaHotel(TipoListaHotel Lista)
{
	return (Lista.Primeiro == Lista.Ultimo);
}

int VaziaVoo(TipoListaVoo Lista)
{
	return (Lista.Primeiro == Lista.Ultimo);
}

int VaziaPasseio(TipoPilhaPasseio Pilha)
{
	return (Pilha.Topo == Pilha.Fundo);
}

void CarregaArquivos(int *num)
{
	FILE  *arq_clientes,*arq_hoteis,*arq_voos,*arq_passeios,*arq_filas;
	char s_clientes[]="clientes.txt",s_hoteis[]="hoteis.txt",s_voos[]="voos.txt",s_passeios[]="passeios.txt",s_fila[]="fila.txt";

   
    TipoItemCliente Cliente;
    TipoItemHotel Hotel;
    TipoItemVoo Voo;
    TipoItemPasseio Passeio;
    TipoItemFilaCliente Fila;

    arq_clientes=fopen(s_clientes, "a+");
	arq_hoteis=fopen(s_hoteis, "a+");
	arq_voos=fopen(s_voos, "a+");
	arq_passeios=fopen(s_passeios, "a+");
	arq_filas=fopen(s_fila, "a+");
        ListaVaziaCliente(&ListaCliente);
	ListaVaziaHotel(&ListaHotel);	
	ListaVaziaVoo(&ListaVoo);
	
	PilhaVaziaPasseio(&PilhaPasseio);
	
	FilaVaziaCliente(&FilaCliente);
	
	while(!feof(arq_clientes))
	{
	    if (fread(&Cliente, sizeof(TipoItemCliente), 1, arq_clientes))
	    {
            erro=InsereCliente(Cliente, &ListaCliente);        
            if(erro==TRUE)
            {
                fclose(arq_clientes);
                fclose(arq_hoteis);
                fclose(arq_voos);
				fclose(arq_passeios);
				fclose(arq_filas);
                exit(1);
            }
        }
	}

	while(!feof(arq_hoteis))
	{
	    if (fread(&Hotel, sizeof(TipoItemHotel), 1, arq_hoteis))
	    {
            erro=InsereHotel(Hotel, &ListaHotel);
            if(erro==TRUE)
            {
                fclose(arq_clientes);
                fclose(arq_hoteis);
                fclose(arq_voos);
				fclose(arq_passeios);
				fclose(arq_filas);
                exit(1);
            }
        }
	}
	while(!feof(arq_voos))
	{
	    if (fread(&Voo, sizeof(TipoItemVoo), 1, arq_voos))
	    {
            erro=InsereVoo(Voo, &ListaVoo);        
            if(erro==TRUE)
            {
                fclose(arq_clientes);
                fclose(arq_hoteis);
                fclose(arq_voos);
				fclose(arq_passeios);
				fclose(arq_filas);
                exit(1);
            }
        }
	}
    while(!feof(arq_passeios))
	{
	    if (fread(&Passeio, sizeof(TipoItemPasseio), 1, arq_passeios))
	    {
            erro=EmpilhaPasseio(&PilhaPasseio,Passeio);
            if(erro==TRUE)
            {
                fclose(arq_clientes);
                fclose(arq_hoteis);
                fclose(arq_voos);
				fclose(arq_passeios);
				fclose(arq_filas);
                exit(1);
            }
        }
	}
    while(!feof(arq_filas))
	{
	    if (fread(&Fila, sizeof(TipoItemFilaCliente), 1, arq_filas))
	    {
            erro= EnfileiraCliente(Fila, &FilaCliente);
            if(erro==TRUE)
            {
                        fclose(arq_clientes);
                        fclose(arq_hoteis);
                        fclose(arq_voos);
			fclose(arq_passeios);
			fclose(arq_filas);
                        exit(1);
             }
                (*num)++;
            }   
	}

    fclose(arq_clientes);
    fclose(arq_hoteis);
    fclose(arq_voos);
    fclose(arq_passeios);
    fclose(arq_filas);
    
    return;
}

void SalvaArquivos()
{
	FILE  *arq_clientes,*arq_hoteis,*arq_voos,*arq_passeios,*arq_filas;
	char s_clientes[]="clientes.txt",s_hoteis[]="hoteis.txt",s_voos[]="voos.txt",s_passeios[]="passeios.txt",s_fila[]="fila.txt";

	
	TipoApontadorCliente auxCliente;
	TipoApontadorHotel auxHotel;
	TipoApontadorVoo auxVoo;
	TipoApontadorPasseio auxPasseio;		
    TipoApontadorFilaCliente auxFila;
     

	arq_clientes=fopen(s_clientes, "w");
	arq_hoteis=fopen(s_hoteis, "w");
	arq_voos=fopen(s_voos, "w");
	arq_passeios=fopen(s_passeios, "w");
	arq_filas=fopen(s_fila, "w");


    auxCliente = ListaCliente.Primeiro->Prox;
    while(auxCliente != NULL)
    {
        fwrite(&(auxCliente->Item), sizeof(TipoItemCliente), 1, arq_clientes);
    	auxCliente = auxCliente->Prox;
	}
    auxHotel = ListaHotel.Primeiro->Prox;
    while(auxHotel != NULL)
    {
        fwrite(&(auxHotel->Item), sizeof(TipoItemHotel), 1, arq_hoteis);
        auxHotel = auxHotel->Prox;
	}
    auxVoo = ListaVoo.Primeiro->Prox;
    while(auxVoo != NULL)
    {
        fwrite(&(auxVoo->Item), sizeof(TipoItemVoo), 1, arq_voos);
        auxVoo = auxVoo->Prox;
	}
	auxPasseio = PilhaPasseio.Topo->Prox;
	 while(auxPasseio != NULL)
    {
        fwrite(&(auxPasseio->Item), sizeof(TipoItemPasseio), 1, arq_passeios);
        auxPasseio = auxPasseio->Prox;
	}
	auxFila = FilaCliente.Frente->Prox;
	while(auxFila != NULL)
	{
        fwrite(&(auxFila->Item), sizeof(TipoItemFilaCliente), 1, arq_filas);
        auxFila = auxFila->Prox;
	}
    fclose(arq_clientes);
	fclose(arq_hoteis);
	fclose(arq_voos);
	fclose(arq_passeios);
	fclose(arq_filas);
	
    return;  
	
	
}



