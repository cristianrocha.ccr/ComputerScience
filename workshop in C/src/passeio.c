#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "data.h"
#include "agturismo.h"
#include "ihm.h"
#include "disco.h"
#include "passeio.h"

void BuscaPasseio(char * nome, TipoApontadorPasseio *Passeio){
	
	*Passeio = PilhaPasseio.Topo->Prox;
	while((*Passeio != NULL)&&( strcmp((*Passeio)->Item.nome, nome)!=0))
    {
	    *Passeio = (*Passeio)->Prox;
	}
}	

void AlterarDadosPasseio()
{
    TipoApontadorPasseio Passeio;
    char nome[TAM_PASSEIO+1];
    int num;
    printf("\nAlterar dados de passeio\n");
	
	LeNomePasseio(nome);
	    
    BuscaPasseio(nome,&Passeio);
    if(Passeio != NULL)
	{
			ImprimePasseio(Passeio);    		        
            printf("Altera agência de turismo? ");
	        if(LeSimNao())
    	    	{
				LeAgencia(Passeio->Item.agencia);
	        }
	        printf("Altera Nome? ");
	        if(LeSimNao())
	        {
	            LeNomePasseio(Passeio->Item.nome);
	        }
	        printf("Altera data?  ");
	        if(LeSimNao())
	        { 
	            LeData(&Passeio->Item.data);
	        }
	        printf("Altera horário de ida? ");
	        if(LeSimNao())
	        {
	        	    LeHorarioIda(&Passeio->Item.horarioIda);
        	}
			printf("Altera horário de chegada? ");
			if(LeSimNao())
	        {
	        	    LeHorarioChegada(&Passeio->Item.horarioChegada);
        	}
        	printf("Altera valor? ");
			if(LeSimNao())
			{
	        	    LeValorPasseio(&Passeio->Item.valor);
			}
			printf("Altera quantidade de lugares? ");
			if(LeSimNao())
			{
	        	    LeLugares(&Passeio->Item.lugares);
			} 	
 	
 	}
	else
	   	 printf("\nNão encontrado\n");

}


int RemovePasseio(char *nome) // a função parte do pressuposto que a PilhaPasseio tem um item com esse nome
//o que é assegurado pela função excluirPasseio
{
	int tam,i = 0;	
    TipoPilhaPasseio PilhaAux;
    TipoItemPasseio Item;
   	PilhaVaziaPasseio(&PilhaAux);
   	
   	do
    {
		DesempilhaPasseio(&PilhaPasseio,&Item);
		erro=EmpilhaPasseio(&PilhaAux,Item);
		if(erro==TRUE)
			return erro;
	}while(strcmp(Item.nome, nome)!=0 );
	
	DesempilhaPasseio(&PilhaAux,&Item);
	
	while(!VaziaPasseio(PilhaAux))
	{
		DesempilhaPasseio(&PilhaAux,&Item);
		erro=EmpilhaPasseio(&PilhaPasseio,Item);
		if(erro==TRUE)
			return erro;	
	}	
	
	return erro;
	
}

void ExcluirPasseio() 
{
    TipoApontadorPasseio  Passeio;
    TipoItemPasseio Item;
    char nome[TAM_PASSEIO+1];
    printf("\nExcluir passeio\n");
	
	LeNomePasseio(nome);	
	    
    BuscaPasseio(nome, &Passeio);
    if(Passeio != NULL)
	{
        ImprimePasseio(Passeio);
		printf("Remove? ");
	        if(LeSimNao()==1)
	        {
	            erro = RemovePasseio(nome);
	            if(erro==FALSE)
					printf("Passeio de nome: %s removido\n",nome);
				else
					printf("Oops. Não foi possível remover");		
	        }
	}
	else
	   	printf("\nNão encontrado\n");
}
int EmpilhaPasseio(TipoPilhaPasseio *Pilha,TipoItemPasseio x){

    TipoApontadorPasseio Aux;
    
    Aux = (TipoApontadorPasseio) malloc(sizeof(TipoCelulaPasseio));
	 if (Aux == NULL)
    {
        printf ("Erro na alocação de passeio\n");
        return erro=TRUE;        
    }
	
	Pilha->Topo->Item = x;
	Aux->Prox = Pilha->Topo;
	Pilha->Topo=Aux;
	Pilha->tamanho++;
	return erro = FALSE;
}

void DesempilhaPasseio(TipoPilhaPasseio *Pilha, TipoItemPasseio *Item)
{ 
    TipoApontadorPasseio q;
    if(VaziaPasseio(*Pilha))
    {
        printf("ERRO, Não é possível excluir pilha\n");
        return;
    }
	q = Pilha->Topo;
	Pilha->Topo = q->Prox;
	*Item = q->Prox->Item;
	free (q);
	Pilha->tamanho--;
}

void IncluirPasseio()
{
    TipoItemPasseio Passeio;
    TipoApontadorPasseio PontCelPasseio;
    
    printf("Inclusão de Passeio\n");
	
	LeNomePasseio(Passeio.nome);
	BuscaPasseio(Passeio.nome, &PontCelPasseio);
	if(PontCelPasseio == NULL)
    {
		LeAgencia(Passeio.agencia);
		LeData(&Passeio.data);
		LeHorarioIda(&Passeio.horarioIda);
		LeHorarioChegada(&Passeio.horarioChegada);
		LeValorPasseio(&Passeio.valor);
		LeLugares(&Passeio.lugares);
		erro=EmpilhaPasseio(&PilhaPasseio,Passeio);
	    if(erro==FALSE)
			printf("\n Incluido\n");
	    else
			printf("\n Não Incluido\n");
    }
    else
    {
		printf("Passeio já está incluso");	
    }
    
    
}

void GerenciamentoPasseios()
{
	int opcao,permanece;
	permanece=TRUE;
	do
	{
		opcao = MenuGerenciamentoPasseios();
		switch (opcao)
		{
	 		case 1:
				IncluirPasseio();
				
				break;
			case 2:
				AlterarDadosPasseio();
			
				break;
			case 3:
				ExcluirPasseio();
				break;
			case 4:  //Retorna 	
				permanece=FALSE;
				break;
			default:
				printf(" Operador invalido");
		}		
	}while (permanece==TRUE);	
}	
