#define TRUE 1
#define FALSE 0

typedef struct
{
	int hora,minutos;

}TipoHorario;

typedef struct
{
	int dia,mes,ano;

}TipoData;

int diferenca(TipoData data1, TipoData data2);
int TestaData(int dia, int mes, int ano);
int AnoBissexto(int ano);
int TestaHoras(int horas);
int TestaMinutos(int minutos);
int TestaDia(int dia);
int TestaMes(int mes);
int TestaAno(int ano);
void DataAtual(TipoData *data);
int DataAnterior_a_Atual(int dia, int mes, int ano);
void CopiaData(TipoData *A, TipoData B);//Copia B em A
void SomaData(TipoData *Data, int dias);
