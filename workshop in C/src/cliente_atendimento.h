TipoApontadorCliente ProcuraCliente(char *cpf,  TipoApontadorCliente *Cliente);
void AlterarDadosCliente();
int InsereCliente (TipoItemCliente x, TipoListaCliente *Lista);
int BuscaCliente(char *cpf);
void IncluirCliente(char *cpf);
void RetiraCliente(TipoApontadorCliente p, TipoListaCliente *Lista, TipoItemCliente *Item);
void ExcluirCliente();
void GerenciamentoClientes();
