void	CarregaArquivos();
void	SalvaArquivos();

void ListaVaziaCliente(TipoListaCliente *Lista);
void ListaVaziaHotel(TipoListaHotel *Lista);
void ListaVaziaVoo(TipoListaVoo *Lista);
void ListaVaziaPasseio(TipoPilhaPasseio *Lista);

int VaziaCliente(TipoListaCliente Lista);
int VaziaHotel(TipoListaHotel Lista);
int VaziaVoo(TipoListaVoo Lista);
int VaziaPasseio(TipoPilhaPasseio Lista);

int InsereCliente (TipoItemCliente x, TipoListaCliente *Lista);
int InsereHotel (TipoItemHotel x, TipoListaHotel *Lista);
int InsereVoo (TipoItemVoo x, TipoListaVoo *Lista);

