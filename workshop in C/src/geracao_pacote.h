int ProcuraHotelCidade(TipoListaHotel *ListaHotelPacote, char *cidade);
void ProcuraVooCidadeDatas(TipoListaVoo *ListaVooPacote, char *origem,char *destino, TipoData data1, TipoData data2);
void MenorPrecoHotel(TipoListaHotel *ListaHotelPacote);
void MenorPrecoVoos(TipoListaVoo *ListaVooPacote, int numDias);
float PrecoIdaVolta (TipoApontadorVoo Voo, int numDias);
void ProcuraHotelPacote(char *nome,  TipoApontadorHotel *Hotel,TipoListaHotel ListaHotelPacote);
void ProcuraVooPacote(char *nomeVoo, TipoApontadorVoo *Voo, TipoListaVoo ListaVooPacote);
void DiminuirLugaresPasseios(TipoPilhaPasseio *PilhaPacotePasseio, int pessoas);
void SelecionaPacote(TipoListaHotel ListaHotelPacote,TipoListaVoo ListaVooPacote,TipoApontadorHotel *Hotel,TipoApontadorVoo *VooIda,TipoApontadorVoo *VooVolta);
void GeracaoPacoteTuristico();

TipoApontadorVoo PontCelVooIda,PontCelVooVolta;
TipoApontadorHotel PontCelHotel;


