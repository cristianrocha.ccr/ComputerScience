void BuscaPasseio(char * nome, TipoApontadorPasseio *Passeio);
void AlterarDadosPasseio();
int RemovePasseio(char *nome);
void ExcluirPasseio(); 
int EmpilhaPasseio(TipoPilhaPasseio *Pilha,TipoItemPasseio x);
void DesempilhaPasseio(TipoPilhaPasseio *Pilha, TipoItemPasseio *Item);
void IncluirPasseio();
void GerenciamentoPasseios();
