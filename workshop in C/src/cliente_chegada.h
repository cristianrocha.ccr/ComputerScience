void FilaVaziaCliente(TipoFilaCliente *Fila);
int VaziaFilaCliente(TipoFilaCliente Fila);
int EnfileiraCliente (TipoItemFilaCliente x, TipoFilaCliente *Fila);
void DesenfileiraCliente(TipoFilaCliente *Fila, char *cpf);
void RegistraChegada(int *num);
void ChegadaCliente(int *num);
int ProcuraFilaCliente(char *cpf);
