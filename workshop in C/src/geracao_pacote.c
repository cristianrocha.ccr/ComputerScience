#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "data.h"
#include "agturismo.h"
#include "disco.h"
#include "ihm.h"  
#include "passeio.h" 
#include "hotel.h"   
#include "voo.h" 
#include "biblioteca.h"  
#include "geracao_pacote.h"

int ProcuraHotelCidade(TipoListaHotel *ListaHotelPacote, char *cidade)
{
	int achou=FALSE;
	TipoApontadorHotel Hotel;
	
	Hotel = ListaHotel.Primeiro->Prox;

    while(Hotel != NULL)
    {
		if( strcmp(Hotel->Item.cidade, cidade) ==0  && Hotel->Item.quartos >=1)//se der tempo incluir quantidade de pessoas
		{
			InsereHotel(Hotel->Item,ListaHotelPacote);
			achou=TRUE;
	   	}
       	Hotel = Hotel->Prox;
	}
	return achou;
}	

void ProcuraVooCidadeDatas(TipoListaVoo *ListaVooPacote, char *origem,char *destino, TipoData data1, TipoData data2)
{
	
	TipoApontadorVoo Voo,VooRetorno,aux;
        int verdade, inseriu;
 	Voo = ListaVoo.Primeiro->Prox;
        inseriu = FALSE;
        while(Voo != NULL)
        {   
                VooRetorno = ListaVoo.Primeiro->Prox;
        	if( strcmp(Voo->Item.origem, origem) ==0 &&  strcmp(Voo->Item.destino, destino) ==0   &&   diferenca(Voo->Item.dataIda,data1)==0 )
		{
                        verdade = FALSE;
			Voo->Item.tipo=IDA;
                        
                        while(VooRetorno!= NULL){
                            if( strcmp(VooRetorno->Item.origem, destino) ==0 &&  strcmp(VooRetorno->Item.destino, origem) ==0  && diferenca(VooRetorno->Item.dataChegada,data2)==0){
                                VooRetorno->Item.tipo=VOLTA;
                                verdade = TRUE;
                                if(inseriu==TRUE)
                                        ProcuraVooPacote(VooRetorno->Item.nomeVoo,&aux,*ListaVooPacote);
                                if((inseriu == FALSE) || (aux==NULL)){
                                    InsereVoo(VooRetorno->Item,ListaVooPacote);
                                    inseriu = TRUE;
                                }    
                             }
                            VooRetorno = VooRetorno->Prox;
                        }
                        if (verdade = TRUE){
                            InsereVoo(Voo->Item,ListaVooPacote);
                            
                        }    
                                                   
                }
				
                Voo = Voo->Prox;      
			
         }
		                    
}	

void MenorPrecoHotel(TipoListaHotel *ListaHotelPacote)
{
	TipoApontadorHotel ant,Hotel;
	TipoItemHotel x;
	float menorPreco;
	
	Hotel = ListaHotelPacote->Primeiro->Prox;

	menorPreco=Hotel->Item.diaria;
    
    while(Hotel != NULL)
    {
		if( Hotel->Item.diaria < menorPreco)
		{
			menorPreco=Hotel->Item.diaria;
		}
		Hotel = Hotel->Prox;      	
	}	

	ant=ListaHotelPacote->Primeiro;
	Hotel = ListaHotelPacote->Primeiro->Prox;
	
	while(Hotel != NULL)
    {
		if( Hotel->Item.diaria > menorPreco)
		{
			RetiraHotel(ant,ListaHotelPacote,&x);
			Hotel=ant->Prox;
		}
		else
		{
			ant=Hotel;
			Hotel = Hotel->Prox;      	
		}
	}	
}	

void MenorPrecoVoos(TipoListaVoo *ListaVooPacote, int numDias)
{
	TipoApontadorVoo VooGer,Voo,ant;
	TipoItemVoo x;
	float menorPreco,preco;
	TipoData data;
	
	//1 parte elimina os maiores valores de cada dia

	ant = (*ListaVooPacote).Primeiro;
	VooGer = (*ListaVooPacote).Primeiro->Prox;
	
	while (VooGer != NULL)
	{
		Voo= VooGer;
		if (Voo->Item.tipo==IDA)
		{
			CopiaData(&data,Voo->Item.dataIda);
			menorPreco=Voo->Item.valor;
		
			while (Voo != NULL)
			{
				if(Voo->Item.tipo==IDA && diferenca(data,Voo->Item.dataIda)==0)
				{
					if(Voo->Item.valor < menorPreco)
						menorPreco=Voo->Item.valor;
				}
				Voo = Voo->Prox;
			}	
			Voo=VooGer;
			while (Voo != NULL)
			{
				
				if(Voo->Item.tipo==IDA && diferenca(data,Voo->Item.dataIda)==0 && Voo->Item.valor > menorPreco )
				{
					RetiraVoo(ant,ListaVooPacote,&x);
					Voo=ant->Prox;
				}
				else
				{
					ant=Voo;
					Voo = Voo->Prox;
				}
			}
		}	
		else
		{
			CopiaData(&data,Voo->Item.dataChegada);
			menorPreco=Voo->Item.valor;
		
			while (Voo != NULL)
			{
				if(Voo->Item.tipo==VOLTA && diferenca(data,Voo->Item.dataChegada)==0)
				{
					if(Voo->Item.valor < menorPreco)
						menorPreco=Voo->Item.valor;
				}
				Voo = Voo->Prox;
			}	
			Voo=VooGer;
			while (Voo != NULL)
			{
				
				if(Voo->Item.tipo==VOLTA && diferenca(data,Voo->Item.dataChegada)==0 && Voo->Item.valor > menorPreco)
				{
					RetiraVoo(ant,ListaVooPacote,&x);
					Voo=ant->Prox;
				}
				else
				{
					ant=Voo;
					Voo = Voo->Prox;
				}
			}
		}
		VooGer=VooGer->Prox;

	}

	//2 parte elimina os maiores valores da soma de dia + numDias
	//2.a verifica o menor preco de dataIda+dataVota
	/*
	menorPreco=INF;	
	Voo = (*ListaVooPacote).Primeiro->Prox;	
	while (Voo != NULL)		
	{
		preco= PrecoIdaVolta (Voo,numDias);
		if ((preco >= 0)&& (preco < menorPreco))
			menorPreco=preco;
			
		Voo = Voo->Prox;
	}
	
	//2.b remove todos os outros
	
	ant = (*ListaVooPacote).Primeiro;	
	Voo = (*ListaVooPacote).Primeiro->Prox;	
	while (Voo != NULL)		
	{
		preco= PrecoIdaVolta (Voo,numDias);
		if ((preco >= 0)&& (preco>menorPreco))
		{
			RetiraVoo(ant,ListaVooPacote,&x);
			Voo=ant->Prox;
		}
		else
		{
			ant=Voo;	
			Voo = Voo->Prox;
		}
	}*/
	
}	

float PrecoIdaVolta (TipoApontadorVoo Voo, int numDias)
{		
		
		float Preco;
		TipoData data,data2;
		
		if (Voo->Item.tipo==IDA)
		{
			CopiaData(&data,Voo->Item.dataIda);
			Preco=Voo->Item.valor;
			
			CopiaData(&data2,data);
			SomaData(&data2,numDias);
			
			while (Voo != NULL && !(Voo->Item.tipo==VOLTA && diferenca(data2,Voo->Item.dataChegada)==0) ) 
			{
				Voo = Voo->Prox;
			}
			if(Voo != NULL)
				Preco=Preco+Voo->Item.valor;
			return Preco;
		}	
		else
			return -1;
}			

void ProcuraHotelPacote(char *nome,  TipoApontadorHotel *Hotel, TipoListaHotel ListaHotelPacote)
{
    *Hotel = ListaHotelPacote.Primeiro->Prox;

    while((*Hotel != NULL)&&(strcmp((*Hotel)->Item.nome, nome)!=0))
    {
       	*Hotel = (*Hotel)->Prox;
	}

}

void ProcuraVooPacote(char *nomeVoo, TipoApontadorVoo *Voo, TipoListaVoo ListaVooPacote)
{
     *Voo = ListaVooPacote.Primeiro->Prox;
	
    while(  (*Voo != NULL)  && (strcmp( (*Voo)->Item.nomeVoo, nomeVoo)!=0))
    {
	   	*Voo = (*Voo)->Prox;
	}
}

void DiminuirLugaresPasseios(TipoPilhaPasseio *PilhaPacotePasseio, int pessoas){
	
	TipoApontadorPasseio Passeio;
	
	Passeio = PilhaPacotePasseio->Topo->Prox;
	while(Passeio !=NULL){
			Passeio->Item.lugares = Passeio->Item.lugares - pessoas;
			Passeio = Passeio->Prox; 
	}	
	
	
}	

void SelecionaPacote(TipoListaHotel ListaHotelPacote,TipoListaVoo ListaVooPacote,TipoApontadorHotel *Hotel,TipoApontadorVoo *VooIda,TipoApontadorVoo *VooVolta){
    char vooIda[TAM_VOO+1],vooVolta[TAM_VOO+1],hotel[TAM_NOME_HOTEL+1];

   
    printf("Digite o nome do hotel que você deseja escolher\n");
    LeGetchar(hotel,TAM_NOME_HOTEL);
    ProcuraHotelPacote(hotel, Hotel, ListaHotelPacote);
    while(*Hotel == NULL)
    {
        printf("Hotel não encontrado\n");
        printf("Digite o nome do hotel que você deseja escolher\n");
        LeGetchar(hotel,TAM_NOME_HOTEL);
        ProcuraHotelPacote(hotel, Hotel, ListaHotelPacote);       
    }   
   
    printf("Digite o nome do voo de ida que você deseja escolher\n");
    LeGetchar(vooIda,TAM_VOO);
    ProcuraVooPacote(vooIda,VooIda,ListaVooPacote);
    
    while(*VooIda == NULL || (*VooIda)->Item.tipo != IDA)
    {
        printf("Voo inválido\n");
        printf("Digite o nome do voo de ida que você deseja escolher\n");
        LeGetchar(vooIda,TAM_VOO);
        ProcuraVooPacote(vooIda,VooIda,ListaVooPacote);
    }   
    
    printf("Digite o nome do voo de volta que você deseja escolher\n");
    LeGetchar(vooVolta,TAM_VOO);
    ProcuraVooPacote(vooVolta,VooVolta,ListaVooPacote);
    while(*VooVolta == NULL ||  (*VooVolta)->Item.tipo != VOLTA)
    {
        printf("Voo inválido\n");
        printf("Digite o nome do voo de volta que você deseja escolher\n");
        LeGetchar(vooVolta,TAM_VOO);
        ProcuraVooPacote(vooVolta,VooVolta,ListaVooPacote);
    }   
   
}

void SelecionaPasseios(TipoPilhaPasseio *PilhaPacotePasseio,TipoData data1,TipoData data2)//
{
        TipoData data;
        TipoApontadorPasseio Passeio;
        char nome[TAM_PASSEIO+1];
        
        CopiaData(&data,data1);
        while(diferenca(data,data2)!=0)
        {
                Passeio = PilhaPasseio.Topo->Prox;
                while(Passeio != NULL)
            {
                    if(diferenca(Passeio->Item.data, data)==0)
                    {        
                            ImprimePasseio(Passeio);        
                                if(PerguntaAcrescentaPasseios())        
                                {
                                        EmpilhaPasseio(PilhaPacotePasseio,Passeio->Item);
                                }
                        }        
                    else
                    {
                          printf("Não há passeios disponíveis para a data");
                           ImprimeData(data);
                    }        
                    Passeio = Passeio->Prox;
                }
                SomaData(&data,1);
        }
        
}
void GeracaoPacoteTuristico()
{
	int numDias,i,dias,pessoas, selecionado;
	TipoData dataA,data1,data2,dataB;
	char origem[TAM_CIDADE+1],destino[TAM_CIDADE+1];
	TipoItemPasseio Passeio;
	TipoItemHotel Hotel;
	TipoItemVoo Voo;
	TipoListaHotel ListaHotelPacote;
	TipoListaVoo ListaVooPacote;
	TipoPilhaPasseio PilhaPacotePasseio;
	TipoApontadorHotel ApHotel;
	TipoApontadorVoo VooIda,VooVolta;
	
	ListaVaziaHotel(&ListaHotelPacote);
	ListaVaziaVoo(&ListaVooPacote);
	
	LeDataAeB(&dataA, &dataB);
	LeQuantidadeDias(&numDias,dataA,dataB);
	LeQuantidadePessoas(&pessoas);
	LeOrigem(origem);
	LeDestino(destino);

	CopiaData(&data1,dataA);
	CopiaData(&data2,dataA);
	SomaData(&data2,numDias);
	
	
	if(!ProcuraHotelCidade(&ListaHotelPacote, destino)) //como os quartos não dependem da data
	{
		printf("\nHoteis não disponiveis\n");	
		return;
	}	
	
	while(diferenca(data1,dataB)>=numDias)
	{
		ProcuraVooCidadeDatas(&ListaVooPacote, origem,destino, data1, data2);//
		SomaData(&data1,1);
		SomaData(&data2,1);
	}
	
	if(VaziaVoo(ListaVooPacote))
	{
		printf("\nVoos não disponiveis\n");	
		return;
	}
	
	MenorPrecoHotel(&ListaHotelPacote);//EXCLUI TODOS OS OUTROS DE MAIOR PREÇO //
	MenorPrecoVoos(&ListaVooPacote, numDias);
		
	ImprimeMenoresPrecos(&ListaHotelPacote, &ListaVooPacote);
        SelecionaPacote(ListaHotelPacote, ListaVooPacote, &ApHotel, &VooIda, &VooVolta);	
	
	while(diferenca(VooIda->Item.dataIda,VooVolta->Item.dataIda)!=numDias){
		printf("Intervalo de datas diferente do requerido pelo cliente. Repita a operação\n");
		SelecionaPacote(ListaHotelPacote, ListaVooPacote, &ApHotel, &VooIda, &VooVolta);
	}	
	
	CopiaData(&data1,VooIda->Item.dataChegada);
	CopiaData(&data2,VooVolta->Item.dataIda);
	SomaData(&data1,1);
	PilhaVaziaPasseio(&PilhaPacotePasseio);
	
	while(PerguntaAcrescentaPasseios())//
	{
		SelecionaPasseios(&PilhaPacotePasseio,data1,data2);//
	}	

	/*if(CompraPacote(ApHotel,VooIda,VooVolta,PilhaPacotePasseio))
	{
		ApHotel->Item.quartos = ApHotel->Item.quartos - 1;
		VooIda->Item.lugares = VooIda->Item.lugares - pessoas;
		VooVolta->Item.lugares = VooVolta->Item.lugares - pessoas;
			if(!PilhaVaziaPasseio(PilhaPacotePasseio))
			DiminuirLugaresPasseios(&PilhaPacotePasseio, pessoas);
	}
	*/
}

