
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "data.h"
#include "agturismo.h"
#include "ihm.h"
#include "disco.h"
#include "voo.h"

TipoApontadorVoo ProcuraVoo(char *nomeVoo, TipoApontadorVoo *Voo)
{
    TipoApontadorVoo VooAnterior;
    
    VooAnterior = ListaVoo.Primeiro;
    *Voo = ListaVoo.Primeiro->Prox;
	
    while(  (*Voo != NULL)  && (strcmp( (*Voo)->Item.nomeVoo, nomeVoo)!=0))
    {
		
       VooAnterior = VooAnterior->Prox;
       	*Voo = (*Voo)->Prox;
	}

	if(*Voo != NULL)
	{
        return VooAnterior;
	}
	else
	    return VooAnterior=NULL;
}



void AlterarDadosVoo()
{
    TipoApontadorVoo Voo;
    int num;
    char nome[TAM_VOO+1];
    TipoData dtIda,dtChegada;
    TipoHorario hrIda,hrChegada;
    printf("\nEntre com os dados do voo que você deseja alterar\n");
	
	LeNomeVoo(nome);
	
	ProcuraVoo(nome,&Voo);
    if(Voo != NULL)
	{
		ImprimeVoo(Voo);    		        
     
			printf("Altera nome do vôo? ");
	        if(LeSimNao())
    	    	{
				LeNomeVoo(Voo->Item.nomeVoo);
	        }
	        printf("Altera Cia? ");
	        if(LeSimNao())
    	    	{
				LeCIA(Voo->Item.cia);
	        }
	        printf("Altera destino? ");
	        if(LeSimNao())
	        {
	            LeDestino(Voo->Item.destino);
	        }
	        printf("Altera origem?  ");
	        if(LeSimNao())
	        {
	            LeOrigem(Voo->Item.origem);
	        }
	        printf("Altera data de ida? ");
	        if(LeSimNao())
	        {
	        	LeDataIda(&Voo->Item.dataIda);
        	}
			printf("Altera horário de ida? ");
			if(LeSimNao())
			{
	            LeHorarioIda(&Voo->Item.horarioIda);
			}
			printf("Altera data de chegada? ");
			if(LeSimNao())
			{
	            LeDataChegada(&Voo->Item.dataChegada);
			}
			printf("Altera horário de chegada? ");
			if(LeSimNao())
			{
	            LeHorarioChegada(&Voo->Item.horarioChegada);
			}
			printf("Altera valor? ");
			if(LeSimNao())
			{
	            LeValorVoo(&Voo->Item.valor);
			}		
			printf("Altera quantidade de lugares? ");
			if(LeSimNao())
			{
	            LeLugares(&Voo->Item.lugares);
			}			
	}
	else
		printf("\nNão encontrado\n");

}

int InsereVoo (TipoItemVoo x, TipoListaVoo *Lista)
{
    Lista->Ultimo->Prox = (TipoApontadorVoo)malloc(sizeof(TipoCelulaVoo));
    if (Lista->Ultimo->Prox == NULL)
    {
        printf ("Erro na alocação de Voo\n");
        return erro=TRUE;        
    }
    Lista->Ultimo = Lista->Ultimo->Prox;
    Lista->Ultimo->Prox = NULL;
    Lista->Ultimo->Item = x;
    return erro=FALSE;    
}

void IncluirVoo()
{
    TipoItemVoo Voo;
    TipoApontadorVoo PontCelVoo;
    
    printf("Inclusão de Voo\n");

	LeNomeVoo(Voo.nomeVoo);
    
    ProcuraVoo(Voo.nomeVoo,&PontCelVoo);
    if(PontCelVoo == NULL)
    {
		
		LeCIA(Voo.cia);
		LeDestino(Voo.destino);
		LeOrigem(Voo.origem);
		LeDataIda(&Voo.dataIda);
		LeHorarioIda(&Voo.horarioIda);	
		LeDataChegada(&Voo.dataChegada);
		LeHorarioChegada(&Voo.horarioChegada);	
		LeValorVoo(&Voo.valor);
		LeLugares(&Voo.lugares);
        erro=InsereVoo(Voo,&ListaVoo);
        if(erro==FALSE)
            printf("\n Incluido\n");
        else
            printf("\n Não Incluido\n");
    }
    else
    {
        printf("\nVoo já está incluso\n");
    }
    return;
}

void RetiraVoo(TipoApontadorVoo p, TipoListaVoo *Lista, TipoItemVoo *Item)
{ // o item a ser retirado é o seguinte ao apontado por p
    TipoApontadorVoo q;
    if(VaziaVoo(*Lista) || p==NULL || p->Prox ==NULL )
    {
        printf("ERRO, Não é possível excluir voo\n");
        return;
    }
    q = p->Prox;
    *Item = q->Item;
    p->Prox = q->Prox;
    if (p->Prox == NULL)
    {
        Lista->Ultimo = p; 
    } 
    free(q);
}

void ExcluirVoo() 
{
    TipoApontadorVoo Anterior, Voo;
    TipoItemVoo Item;
    int remove,num;
    char nome[TAM_VOO+1];
    TipoData dtIda,dtChegada;
    TipoHorario hrIda,hrChegada;
    printf("\nEntre com o nome do voo que você deseja excluir\n");
	
	LeNomeVoo(nome);
	    
    Anterior=ProcuraVoo(nome,&Voo);
    if(Voo != NULL)
	{
        ImprimeVoo(Voo);
		printf("Remove? ");
	        if(LeSimNao()==1)
	        {
	            RetiraVoo(Anterior, &ListaVoo, &Item);
	            printf("Voo removido\n");     
	        }
	}
	else
	   	printf("\nNão encontrado\n");
}

void GerenciamentoVoos()
{
	int opcao,permanece;
	permanece=TRUE;
	do
	{
		opcao = MenuGerenciamentoVoos();
		switch (opcao)
		{
	 		case 1:
				IncluirVoo();
				break;
			case 2:	
				AlterarDadosVoo();
				break;
			case 3:
				ExcluirVoo();
				break;
			case 4: //Retorna
				permanece=FALSE;
				break;
			default:
				printf(" Operador invalido");
		}		
	}while (permanece==TRUE);	
}	
