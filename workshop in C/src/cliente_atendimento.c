#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "data.h"
#include "agturismo.h"
#include "ihm.h"
#include "disco.h"


TipoApontadorCliente ProcuraCliente(char *cpf,  TipoApontadorCliente *Cliente)
{
    TipoApontadorCliente ClienteAnterior;
    
    ClienteAnterior = ListaCliente.Primeiro;
    *Cliente = ListaCliente.Primeiro->Prox;

    while((*Cliente != NULL)&&( strcmp( (*Cliente)->Item.cpf, cpf)!=0 ))
    {
        ClienteAnterior = ClienteAnterior->Prox;
       	*Cliente = (*Cliente)->Prox;
	}

	if(*Cliente != NULL)
	{
        return ClienteAnterior;
	}
	else
	    return ClienteAnterior=NULL;
}

void AlterarDadosCliente()
{
    TipoApontadorCliente Cliente;
    char cpf[TAM_CPF+1];
    int num;
    printf("\nAlterar dados de cliente\n");
	
	LeCPF(cpf);
	    
    ProcuraCliente(cpf, &Cliente);
    if(Cliente != NULL)
	{
		ImprimeCliente(Cliente);    		        
     
	 	  
	        printf("Altera CPF? ");
	        if(LeSimNao())
    	    	{
				LeCPF(Cliente->Item.cpf);
	        }
	        printf("Altera Nome? ");
	        if(LeSimNao())
	        {
	            LeNomeCliente(Cliente->Item.nome);
	        }
	        printf("Altera endereço?  ");
	        if(LeSimNao())
	        {
	            LeEnderecoCliente(Cliente->Item.endereco);
	        }
	        printf("Altera telefone? ");
	        if(LeSimNao())
	        {
	        	    LeTelefone(Cliente->Item.telefone);
        	}
			printf("Altera email? ");
			if(LeSimNao())
	        {
	        	    LeEnderecoCliente(Cliente->Item.endereco);
        	}
 	}
	else
	   	 printf("\nNão encontrado\n");

}


int InsereCliente (TipoItemCliente x, TipoListaCliente *Lista)
{
    Lista->Ultimo->Prox = (TipoApontadorCliente)malloc(sizeof(TipoCelulaCliente));
    if (Lista->Ultimo->Prox == NULL)
    {
        printf ("Erro na alocação de cliente\n");
        return erro=TRUE;        
    }
    Lista->Ultimo = Lista->Ultimo->Prox;
    Lista->Ultimo->Prox = NULL;
    Lista->Ultimo->Item = x;
    return erro=FALSE;    
}

int BuscaCliente(char *cpf)
{
    TipoApontadorCliente Cliente;
    int ok;
    
    printf("\nBusca de cliente pelo CPF %s\n",cpf);
    
    // LeCPF(cpf);
    ProcuraCliente(cpf, &Cliente);
    
    if(Cliente != NULL)
	{
    	ImprimeCliente(Cliente);
		return ok=TRUE;
	}
	printf("\nNão encontrado\n");
	return ok=FALSE;    
}

void IncluirCliente(char *cpf)
{
    TipoItemCliente Cliente;
    
    printf("Inclusão de Cliente\n");

	strcpy(Cliente.cpf,cpf);
	LeNomeCliente(Cliente.nome);
	LeEnderecoCliente(Cliente.endereco);
	LeTelefone(Cliente.telefone);
	LeEmail(Cliente.email);
    erro=InsereCliente(Cliente,&ListaCliente);
    if(erro==FALSE)
		printf("\n Incluido\n");
    else
		printf("\n Não Incluido\n");
    return;
}


void RetiraCliente(TipoApontadorCliente p, TipoListaCliente *Lista, TipoItemCliente *Item)
{ // o item a ser retirado é o seguinte ao apontado por p
    TipoApontadorCliente q;
    if(VaziaCliente(*Lista) || p==NULL || p->Prox ==NULL )
    {
        printf("ERRO, Não é possível excluir cliente\n");
        return;
    }
    q = p->Prox;
    *Item = q->Item;
    p->Prox = q->Prox;
    if (p->Prox == NULL)
    {
        Lista->Ultimo = p; 
    } 
    free(q);
}

void ExcluirCliente() 
{
    TipoApontadorCliente Anterior, Cliente;
    TipoItemCliente Item;
    int remove,num;
    char cpf[TAM_CPF+1];
    printf("\nExcluir cliente\n");
	
	LeCPF(cpf);	
	    
    Anterior=ProcuraCliente(cpf, &Cliente);
    if(Cliente != NULL)
	{
        ImprimeCliente(Cliente);
		printf("Remove? ");
	        if(LeSimNao()==1)
	        {
	            RetiraCliente(Anterior, &ListaCliente, &Item);
	            printf("Cliente com cpf: %s removido\n",Item.cpf);     
	        }
		}
		else
	    	printf("\nNão encontrado\n");
}
	
void GerenciamentoClientes()
{
	int opcao,permanece;
	permanece=TRUE;
	do
	{
		opcao = MenuGerenciamentoClientes();
		switch (opcao)
		{
	 		case 1:
				AlterarDadosCliente();
				break;
			case 2:
				ExcluirCliente();
				break;
			case 3: //Retorna
				permanece=FALSE;
				break;
			default:
				printf(" Operador invalido");
		}		
	}while (permanece==TRUE);	
}	
