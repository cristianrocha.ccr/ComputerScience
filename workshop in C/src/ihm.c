#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "data.h"
#include "agturismo.h"
#include "ihm.h"
#include "biblioteca.h"




//~Em construção

//========MENUS============

int Menu()
{
	int opcao;
/*	system("clear"); */
	printf("\n Menu principal: \n");
	printf("  1. Registrar chegada do cliente\n");
	printf("  2. Atender cliente\n");
	printf("  3. Gerenciamento de hotéis\n");
	printf("  4. Gerenciamento de vôos\n");
	printf("  5. Gerenciamento de passeios\n");
	printf("  6. Terminar Programa\n");
	printf("\n Digite a opção:  ");
	
	LeNumero(&opcao);
	printf("\n");
	return opcao;
}		

int MenuCliente()
{
	int opcao;
	//~ system("clear"); 
	printf("\n Menu Atendimento Clientes: \n");
	printf("  1. Gerenciamento de Clientes\n");
	printf("  2. Geração do pacote Turístico\n");
	printf("  3. Retorna\n");
	printf("\n Digite a opção:  ");
	
	LeNumero(&opcao);
	printf("\n");
	return opcao;

}

int MenuGerenciamentoClientes()
{
	int opcao;
	printf("\n Menu Gerenciamento Clientes:\n");
	printf("  1. Alterar dados de um cliente.\n");
	printf("  2. Excluir um cliente.\n");
	printf("  3. retorna.\n");
	printf("\n Digite a opção:  ");
	LeNumero(&opcao);
	printf("\n");
	return opcao;
}

int MenuGerenciamentoHoteis()
{
	int opcao;
	printf("\n Menu Gerenciamento de Hoteis:\n");
	printf("  1. Incluir Hotel.\n");
	printf("  2. Alterar dados de um Hotel.\n");
	printf("  3. Excluir um Hotel.\n");
	printf("  4. retorna.\n");
	printf("\n Digite a opção:  ");
	LeNumero(&opcao);
	printf("\n");
	return opcao;
}

int MenuGerenciamentoVoos()
{
	int opcao;
	printf("\n Menu Gerenciamento de Vôos:\n");
	printf("  1. Incluir vôo.\n");
	printf("  2. Alterar dados de um vôo.\n");
	printf("  3. Excluir um vôo.\n");
	printf("  4. retorna.\n");
	printf("\n Digite a opção:  ");
	LeNumero(&opcao);
	printf("\n");
	return opcao;
}

int MenuGerenciamentoPasseios()
{
	int opcao;
	printf("\n Menu Gerenciamento de Passeios:\n");
	printf("  1. Incluir Passeio.\n");
	printf("  2. Alterar dados de um Passeio.\n");
	printf("  3. Excluir um Passeio.\n");
	printf("  4. retorna.\n");
	printf("\n Digite a opção:  ");
	LeNumero(&opcao);
	printf("\n");
	return opcao;
}

int MenuChegadaCliente() //BONUS
{
	int opcao;
	printf("\n Menu Chegada Clientes:\n");
//	printf("  1. Clientes ESPECIAIs. \n");
//	printf("  2. Clientes VIP.\n");
	printf("  1. Clientes.\n");//Normais
	printf("  2. retorna.\n");
	printf("\n Digite a opção:  ");
	LeNumero(&opcao);
	printf("\n");
	return opcao;
}	
//======Imprime=============

void ImprimeCliente( TipoApontadorCliente Cliente )
{
    printf("CPF: %s\n",Cliente->Item.cpf);
    printf("Nome: %s\n",Cliente->Item.nome);
    printf("Endereço: %s\n",Cliente->Item.endereco);
    printf("Telefone: %s\n",Cliente->Item.telefone);
    printf("Email: %s\n",Cliente->Item.email);
    return;  
}
void ImprimeHotel( TipoApontadorHotel Hotel )
{
    printf("Nome: %s\n",Hotel->Item.nome);
    printf("Endereço: %s\n",Hotel->Item.endereco);
    printf("Cidade: %s\n",Hotel->Item.cidade);
    printf("Valor da diaria: %.2f\n",Hotel->Item.diaria);
    printf("Número de quartos disponíveis: %d\n",Hotel->Item.quartos);
    return;  
}
void ImprimeVoo( TipoApontadorVoo Voo )
{
	printf("Nome: %s\n", Voo->Item.nomeVoo);
        printf("CIA: %s\n",Voo->Item.cia);
	printf("Origem: %s\n",Voo->Item.origem);
	printf("Destino: %s\n",Voo->Item.destino);
	printf("Data de Ida: %d - %d - %d\n",Voo->Item.dataIda.dia,Voo->Item.dataIda.mes,Voo->Item.dataIda.ano);
	printf("Horario de Ida: %d:%d \n",Voo->Item.horarioIda.hora,Voo->Item.horarioIda.minutos);
	printf("Data de Chegada: %d - %d - %d\n",Voo->Item.dataChegada.dia,Voo->Item.dataChegada.mes,Voo->Item.dataChegada.ano);
        printf("Horario de Chegada: %d:%d \n",Voo->Item.horarioChegada.hora,Voo->Item.horarioChegada.minutos);
	printf("Valor: %.2f\n",Voo->Item.valor);
	printf("Lugares disponiveis: %d\n",Voo->Item.lugares);
        
    return;  
}

void ImprimePasseio( TipoApontadorPasseio Passeio )
{
    printf("Agencia: %s\n",Passeio->Item.agencia);
    printf("Nome: %s\n",Passeio->Item.nome);
    printf("Data: %d - %d - %d\n",Passeio->Item.data.dia,Passeio->Item.data.mes,Passeio->Item.data.ano);    
    printf("Horario de Ida: %d : %d \n",Passeio->Item.horarioIda.hora,Passeio->Item.horarioIda.minutos);
    printf("Horario de Chegada: %d : %d \n",Passeio->Item.horarioChegada.hora,Passeio->Item.horarioChegada.minutos);
    printf("Valor: %.2f\n",Passeio->Item.valor);
	printf("Lugares disponiveis: %d\n",Passeio->Item.lugares);
    
    return;  
}
// ==== LEITURA DE DADOS =====
int LeSimNao()
{
	int alternativa;
	printf("(Se Não digite 0, se Sim digite 1):  ");
    erro=scanf("%d",&alternativa);
	enter();
	while ((erro==0)||(  (alternativa!=1)&&(alternativa!=0)   ))
	{
		printf("Operador inválido\n (Se Não digite 0, se Sim digite 1):  ");
	    erro=scanf("%d",&alternativa);
		enter();
	}
	
	return alternativa;
}

void LeCPF(char *cpf)
{
   	do
    {
	   	printf("Digite o CPF(apenas números, %d dígitos): ",TAM_CPF);
	    LeGetchar(cpf,TAM_CPF);
    }while(!TestaCPF(cpf));
}
void LeNomeCliente(char *nome)
{
	printf("Digite o nome (%d caracteres no max): ",TAM_NOME);
    LeGetchar(nome,TAM_NOME);
}
void LeEnderecoCliente(char *endereco)
{
	printf("Digite o endereço(%d caracteres no max): ",TAM_ENDERECO);
    LeGetchar(endereco,TAM_ENDERECO);
}
void LeNomeHotel(char *nome)
{
	printf("Digite o nome do hotel (%d caracteres no max): ",TAM_NOME_HOTEL);
    LeGetchar(nome,TAM_NOME_HOTEL);
}

void LeNomePasseio(char *nome)
{
	printf("Digite o nome do passeio (%d caracteres no max): ",TAM_PASSEIO);
    LeGetchar(nome,TAM_PASSEIO);
}
void LeEnderecoHotel(char *endereco)
{
	printf("Digite o endereço do hotel (%d caracteres no max): ",TAM_ENDERECO_HOTEL);
    LeGetchar(endereco,TAM_ENDERECO_HOTEL);
}

void LeTelefone(char *telefone)
{
	printf("Digite o telefone no formato (DDD)xxxx-xxxx: ");
	LeGetchar(telefone,TAM_TELEFONE);
}
void LeEmail(char *email)
{
	printf("Digite o email (%d caracteres no max): ",TAM_EMAIL);
	LeGetchar(email,TAM_EMAIL);
}

void LeCidade(char *cidade)
{
	printf("Digite o nome da Cidade (%d caracteres no max): ",TAM_CIDADE);
    LeGetchar(cidade,TAM_CIDADE);
}

void LeDiaria(float *diaria)
{
	printf("Digite a diaria:  ");
	LeNumeroReal(diaria);			
}

void LeQuartos(int *quartos)
{
	printf("Digite o número de quartos disponiveis:  ");
	LeNumero(quartos);			
}

void LeHorario(TipoHorario *horario) // TESTAR
{
	int horas,minutos;
	do 
	{
		printf("Digite as horas, 0 até 23:  ");
		LeNumero(&horas);			
	}while(!TestaHoras(horas));
	
	do
	{
		printf("Digite os minutos, 0 até 59 :  ");
		LeNumero(&minutos);			
	}while(!TestaMinutos(minutos));
	
	(*horario).hora=horas;
	(*horario).minutos=minutos;
}

void LeQuantidadeDias(int *num, TipoData A, TipoData B)
{
	printf("Digite o número de dias da viagem:  ");
	LeNumero(num);
	while(diferenca(A,B) < *num)
	{
		printf("\n Quantidade de dias maior do que a diferênça entre as datas\n");
		printf("Digite o número de dias da viagem: ");
		LeNumero(num);
	}	

}	

void LeQuantidadePessoas(int *pessoas)
{
	printf("Digite a quantidade de pessoas\n");
	LeNumero(pessoas);
}	

void LeData(TipoData *data)// TESTAR
{
	int dia,mes,ano;
	do
	{
		do
		{
			printf("\nDigite o dia:  ");
			LeNumero(&dia);			
		}while(!TestaDia(dia));

		do
		{
			printf("\nDigite o mês:  ");
			LeNumero(&mes);			
		}while(!TestaMes(mes));

		do
		{
			printf("\nDigite o ano:  ");
			LeNumero(&ano);			
		}while(!TestaAno(ano));

	}while( !TestaData(dia,mes,ano) || DataAnterior_a_Atual(dia,mes,ano));
	
	(*data).dia=dia;
	(*data).mes=mes;
	(*data).ano=ano;
}

void LeHorarioIda(TipoHorario *ida)
{
	printf("\nDigite o horario de ida  ");
	LeHorario(ida);	
}	

void LeHorarioChegada(TipoHorario *chegada)
{
	printf("\nDigite o horario de chegada  ");
	LeHorario(chegada);	
}	

void LeDataIda(TipoData *ida)
{
	printf("\nDigite a data de ida  ");
	LeData(ida);	
}	

void LeDataChegada(TipoData *chegada)
{
	printf("\nDigite a data de chegada  ");
	LeData(chegada);	
}	

void LeDataVolta(TipoData *volta)
{
	printf("\nDigite a data de volta  ");
	LeData(volta);	
}	

void LeDataAeB(TipoData *A, TipoData *B)
{
	LeDataIda(A);
	LeDataChegada(B);
	while(diferenca(*A,*B) < 0)
	{
		printf("\nA data de ida é maior do que a data de volta");
		LeDataIda(A);
		LeDataVolta(B);
	}	
}	

void LeCIA(char *cia)
{
	printf("Digite o nome da Companhia Aérea (%d caracteres no max): ",TAM_CIA);
    LeGetchar(cia,TAM_CIA);
}	

void LeDestino(char *destino)
{
	printf("Digite o nome da cidade de destino (%d caracteres no max): ",TAM_CIDADE);
    LeGetchar(destino,TAM_CIDADE);
}

void LeOrigem(char *origem)
{
	printf("Digite o nome da cidade de origem (%d caracteres no max): ",TAM_CIDADE);
    LeGetchar(origem,TAM_CIDADE);
}

void LeValorVoo(float *valor)
{
	printf("Digite o valor do Vôo:  ");
	LeNumeroReal(valor);			
}

void LeValorPasseio(float *valor)
{
	printf("Digite o valor do Passeio:  ");
	LeNumeroReal(valor);			
}

void LeLugares(int *lugares)
{
	printf("Digite o número de lugares disponíveis:  ");
	LeNumero(lugares);			
}

void LeAgencia(char *agencia)
{
	printf("Digite o nome da agencia de turismo (%d caracteres no max): ",TAM_AGENCIA);
    LeGetchar(agencia,TAM_AGENCIA);
}	

void LeNomeVoo(char *voo)
{
	printf("Digite o nome do voo (%d caracteres no max): ",TAM_VOO);
    LeGetchar(voo,TAM_VOO);
}	
void ImprimeMenoresPrecos(TipoListaHotel *ListaHotelPacote,TipoListaVoo *ListaVooPacote){
	
	TipoApontadorHotel Hotel;
	TipoApontadorVoo Voo;
	Hotel = ListaHotelPacote->Primeiro->Prox;
	while( Hotel!= NULL){
		ImprimeHotel(Hotel);
                printf("\n");
		Hotel = Hotel->Prox;
	}	
	Voo = ListaVooPacote->Primeiro->Prox;
	while( Voo!= NULL ){
		ImprimeVoo(Voo);
                printf("\n");
		Voo = Voo->Prox;
	}	
}	

int CompraPacote(TipoApontadorHotel Hotel,TipoApontadorVoo VooIda,TipoApontadorVoo VooVolta,TipoPilhaPasseio PilhaPacotePasseio){
	TipoApontadorPasseio Passeio;
	
	printf("\n");
	printf("HOTEL:\n");
	ImprimeHotel(Hotel);
	printf("\n");
	printf("VOO IDA:\n");
	ImprimeVoo(VooIda);
	printf("VOO VOLTA:\n");
	ImprimeVoo(VooVolta);
	printf("\n");
	printf("PASSEIOS:\n");
	Passeio = PilhaPacotePasseio.Topo->Prox;
	while(Passeio!=NULL){
		ImprimePasseio(Passeio);
		Passeio = Passeio->Prox;	
	}		
	printf("\n");
	printf("Você deseja comprar o pacote com os itens acima?\n");
	if(LeSimNao())
		return TRUE;
	else
		return FALSE;	

}

int PerguntaAcrescentaPasseios()
{
	int ok;
	printf("Gostaria de acrescentar passeios?\n");
	if(LeSimNao())
	 return ok=TRUE;
	return ok=FALSE;	
}	
