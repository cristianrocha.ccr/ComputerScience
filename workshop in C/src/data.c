#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#include "data.h"

//CALCULAR DIFERENCA ENTRE DUAS DATAS
int diferenca(TipoData data1, TipoData data2)
{
	struct tm tm1, tm2;
	time_t t1, t2;
	long d;
	tm1.tm_mday  = data1.dia;
	tm1.tm_mon = data1.mes - 1;
	tm1.tm_year = data1.ano - 1900;
	tm1.tm_hour = tm1.tm_min = tm1.tm_sec = 0;
	tm1.tm_isdst = -1;


	tm2.tm_mday = data2.dia;
	tm2.tm_mon = data2.mes - 1;
	tm2.tm_year = data2.ano - 1900;
	tm2.tm_hour = tm2.tm_min = tm2.tm_sec = 0;
	tm2.tm_isdst = -1;

	t1 = mktime(&tm1);
	t2 = mktime(&tm2);
	
	if(t1 == -1 || t2 == -1)
		printf("erro com mktime\n");
	else {
		d =  (difftime(t2, t1)) / 86400L;
		return  d;
	}
}

int TestaData(int dia, int mes, int ano)
{
    int valido = TRUE;
    int tam_mes[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    
    if ( AnoBissexto(ano) )
    {
        tam_mes[2] = 29;
    }
    
    if ( mes < 1 || mes > 12 )
        valido = FALSE;
    else if ( dia < 1 || dia > tam_mes[mes] )
        valido = FALSE;
    
    if(valido==FALSE)
		printf("\nData inválida\n");
		
    return valido;
}

void CopiaData(TipoData *A, TipoData B)//Copia B em A
{
		(*A).dia=B.dia;
		(*A).mes=B.mes;
		(*A).ano=B.ano;
}	

void SomaData(TipoData *data, int dias)
{
    int i,tam_mes[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    TipoData D;
    D.dia=(*data).dia;
    D.mes=(*data).mes;
    D.ano=(*data).ano;
    
    if ( AnoBissexto(D.ano) )
        tam_mes[2] = 29;
	
	for(i=1;i<=dias;i++)
	{
		D.dia++;
		if(D.dia==(tam_mes[D.mes]+1))
		{	
			D.dia=1;
			D.mes++;
		}
		if(D.mes==13)
		{
			D.mes=1;
			D.ano++;
		}
		if( AnoBissexto(D.ano) )
			tam_mes[2] = 29;
		else
			tam_mes[2] = 28;	
	}

    (*data).dia=D.dia;
    (*data).mes=D.mes;
    (*data).ano=D.ano;

}	




int DataAnterior_a_Atual(int dia, int mes, int ano)
{
    int dataAnterior,dif;
    TipoData dataAtual, dataDigitada;
		
	dataDigitada.dia=dia;
	dataDigitada.mes=mes;
	dataDigitada.ano=ano;
	
	DataAtual(&dataAtual);
	dif= diferenca(dataAtual,dataDigitada);
	if (dif >= 0)
	{
		return dataAnterior = FALSE;
		
	}	 
	printf("Data anterior a atual\n");
	return dataAnterior = TRUE;

}

    
int AnoBissexto(int ano)
{
    int result;
    
    if((ano%4) != 0)           
        result = FALSE;            
    else if ( (ano%400) == 0 )   
        result = TRUE;             
    else if ( (ano%100) == 0 )    
        result = FALSE;            
    else                           
        result = TRUE;            
    
    return  result ;
}

int TestaHoras(int horas)
{
	int valido;
	if(horas>=0 && horas<=23)
	{
		return valido=TRUE;
	}
	
	printf("\nHora inválida\n");
	return valido=FALSE; 

}	
int TestaMinutos(int minutos)
{
	int valido;
	if(minutos>=0 && minutos<=59)
	{
		return valido=TRUE;
	}
	
	printf("\nMinutos inválidos\n");
	return valido=FALSE; 
}	

int TestaDia(int dia)
{
	int valido;
	if(dia>=1 && dia<=31)
	{
		return valido=TRUE;
	}
	
	printf("\nDia inválido\n");
	return valido=FALSE; 

}	
int TestaMes(int mes)
{
	int valido;
	if(mes>=1 && mes<=12)
	{
		return valido=TRUE;
	}
	
	printf("\nMês inválido\n");
	return valido=FALSE; 
}	
int TestaAno(int ano)
{
	int valido;
	if(ano>=2000 && ano<=2100)
	{
		return valido=TRUE;
	}
	
	printf("\nAno inválido, intervalo de 2000 a 2100\n");
	return valido=FALSE; 
}	

void DataAtual(TipoData *data)
{
	int mes,ano,dia;
     
    struct tm *Sys_Time = NULL;
     
    time_t Tval = 0;
    Tval = time(NULL);
    Sys_Time = localtime(&Tval);
     
     
    dia=Sys_Time->tm_mday;
    mes=Sys_Time->tm_mon+1;
    ano=1900 + Sys_Time->tm_year;
	
	data->dia = dia;
	data->mes = mes;
	data->ano = ano;
}

void ImprimeData(TipoData data)
{
	printf("data: %d / %d / %d\n",data.dia, data.mes, data.ano);
}
