#define TRUE 1
#define FALSE 0
#define SIM 1
#define NAO 0
#define IDA 1
#define VOLTA 0
#define INF 9999999

#define TAM_CPF 11
#define TAM_NOME 100
#define TAM_ENDERECO 100
#define TAM_TELEFONE 14
#define TAM_EMAIL 30
#define TAM_NOME_HOTEL 50
#define TAM_ENDERECO_HOTEL 100
#define TAM_CIDADE 20
#define TAM_VOO 10
#define TAM_CIA 50
#define	TAM_AGENCIA 20 
#define	TAM_PASSEIO 100


//• Cliente: cpf, nome (100 caracteres), endereco (100 caracteres), telefone (numero e DDD), email(30 caracteres) e milhagem (numérico long).
typedef struct
{
	char cpf[TAM_CPF+1],nome[TAM_NOME+1],endereco[TAM_ENDERECO+1], telefone[TAM_TELEFONE+1], email[TAM_EMAIL+1];

}TipoItemCliente;

//• Hotéis: nome(50 caracteres), endereço(100 caracteres), cidade (20 caracteres), valor das diárias, quantidade de quartos disponíveis.
typedef struct
{
	char nome[TAM_NOME_HOTEL+1],endereco[TAM_ENDERECO_HOTEL+1],cidade[TAM_CIDADE+1];
	float diaria;
	int quartos;
}TipoItemHotel;

//• Vôos aéreas: cia. aérea(50 caracteres), destino(20 caracteres), origem(20 caracteres), data e horário de ida, data e horário de chegada, valor, quantidade de lugares disponíveis.


//struct TipoData e TipoHorario definido em data.h 

typedef struct
{
	char nomeVoo[TAM_VOO+1], cia[TAM_CIA+1],destino[TAM_CIDADE+1],origem[TAM_CIDADE+1];
	TipoData dataIda,dataChegada;
	TipoHorario horarioIda,horarioChegada;
	float valor;
	int lugares;
	int tipo;//1 se for ida, 0 se for volta
}TipoItemVoo;

//• Passeios: agência de turismo (20 caracteres), nome do passeio (100 caracteres), data, horário de ida, horário de volta,
//valor, quantidade de lugares disponíveis.

typedef struct
{
	char agencia[TAM_AGENCIA+1],nome[TAM_PASSEIO+1];
	TipoData data;
	TipoHorario horarioIda,horarioChegada;
	float valor;
	int lugares;

}TipoItemPasseio;

typedef struct TipoCelulaCliente *TipoApontadorCliente;


typedef struct TipoCelulaCliente
{
	TipoItemCliente Item;
	TipoApontadorCliente Prox;		

}TipoCelulaCliente;

typedef struct 
{
	TipoApontadorCliente Primeiro, Ultimo;
}TipoListaCliente;

typedef struct TipoCelulaHotel *TipoApontadorHotel;

typedef struct TipoCelulaHotel
{
	TipoItemHotel Item;
	TipoApontadorHotel Prox;		
}TipoCelulaHotel;

typedef struct 
{
	TipoApontadorHotel Primeiro, Ultimo;
}TipoListaHotel;

typedef struct TipoCelulaPasseio *TipoApontadorPasseio;

typedef struct TipoCelulaPasseio
{
	TipoItemPasseio Item;
	TipoApontadorPasseio Prox;		

}TipoCelulaPasseio;

typedef struct 
{
	TipoApontadorPasseio Topo,Fundo;
	int tamanho;
}TipoPilhaPasseio;

typedef struct TipoCelulaVoo *TipoApontadorVoo;

typedef struct TipoCelulaVoo
{
	TipoItemVoo Item;
	TipoApontadorVoo Prox;		

}TipoCelulaVoo;

typedef struct 
{
	TipoApontadorVoo Primeiro, Ultimo;
}TipoListaVoo;


typedef struct
{
	char cpf[TAM_CPF+1];

}TipoItemFilaCliente;

typedef struct TipoCelulaFilaCliente *TipoApontadorFilaCliente;

typedef struct TipoCelulaFilaCliente
{
	TipoItemFilaCliente Item;
	TipoApontadorFilaCliente Prox;		

}TipoCelulaFilaCliente;

typedef struct 
{
	TipoApontadorFilaCliente Frente, Tras;
}TipoFilaCliente;

TipoFilaCliente FilaCliente;

TipoListaCliente ListaCliente;
TipoListaHotel ListaHotel;
TipoListaVoo ListaVoo;
TipoPilhaPasseio PilhaPasseio;


int erro;
