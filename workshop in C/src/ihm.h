int Menu();
int MenuCliente();
int MenuGerenciamentoClientes();
int MenuGerenciamentoHoteis();
int MenuGerenciamentoVoos();
int MenuGerenciamentoPasseios();
int MenuChegadaCliente();
void ImprimeCliente( TipoApontadorCliente Cliente );
void ImprimeHotel( TipoApontadorHotel Hotel );
void ImprimeVoo( TipoApontadorVoo Voo );
void ImprimePasseio( TipoApontadorPasseio Passeio );;
int LeSimNao();
void LeCPF(char *cpf);
void LeNomeCliente(char *nome);
void LeEnderecoCliente(char *endereco);
void LeNomeHotel(char *nome);
void LeNomePasseio(char *nome);
void LeEnderecoHotel(char *endereco);
void LeTelefone(char *telefone);
void LeEmail(char *email);
void LeCidade(char *cidade);
void LeDiaria(float *diaria);
void LeQuartos(int *quartos);
void LeHorario(TipoHorario *horario);
void LeQuantidadeDias(int *num, TipoData A, TipoData B);
void LeQuantidadePessoas(int *pessoas);
void LeData(TipoData *data);
void LeHorarioIda(TipoHorario *ida);
void LeHorarioChegada(TipoHorario *chegada);
void LeDataIda(TipoData *ida);
void LeDataChegada(TipoData *chegada);
void LeDataVolta(TipoData *volta);
void LeDataAeB(TipoData *A, TipoData *B);
void LeCIA(char *cia);
void LeDestino(char *destino);
void LeOrigem(char *origem);
void LeValorVoo(float *valor);
void LeValorPasseio(float *valor);
void LeLugares(int *lugares);
void LeAgencia(char *agencia);
void LeNomeVoo(char *voo);
int CompraPacote(TipoApontadorHotel Hotel,TipoApontadorVoo VooIda,TipoApontadorVoo VooVolta,TipoPilhaPasseio PilhaPacotePasseio);
void ImprimeMenoresPrecos(TipoListaHotel *ListaHotelPacote,TipoListaVoo *ListaVooPacote);
int PerguntaAcrescentaPasseios();
