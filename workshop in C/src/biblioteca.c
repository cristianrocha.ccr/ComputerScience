#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "data.h"
#include "agturismo.h"

void enter() //para colocar após os scanf
{
	char c;
	c=getchar();
	while (c!='\n')
	{
		c=getchar();
	}
}

void LeGetchar(char *nome, int tam)
{
    int i;
    char c;
    
    for (i=0; ( c=getchar() )!='\n'  ;i++)
    {
		if (i<tam)
		{
        	nome[i]=c;
        }
    }
   
    while(i==0)
    {
		printf("Não foi digitado nenhum caracter, digite novamente: ",tam);
	    for (i=0; ( c=getchar() )!='\n'  ;i++)
	    {
			if (i<tam)
			{
	        	nome[i]=c;
	        }
	    }
		
    }
    
    while(i>tam)
    {
		printf("Número máximo de caracteres %d\nDigite novamente: ",tam);
	    for (i=0; ( c=getchar() )!='\n'  ;i++)
	    {
			if (i<tam)
			{
	        	nome[i]=c;
	        }
	    }
		
    }
    nome[i]='\0';
    
}

void LeNumero(int *numero)
{
	int ok;
	ok=scanf("%d",numero);			
	enter();

	while(ok==0)
	{
		printf("\nDigito inválido\n Digite novamente:  ");
		ok=scanf("%d",numero);			
		enter();
	}	
}

int VerificaVirgula(int ok)
{
	int virgula;
	char c;
	
	c=getchar();
	if(ok==1 && c==',')
	{
		printf("As casas decimais devem ser indicadas por ponto. Exemplo 25.50");
		enter();
		return virgula=TRUE;
	}	
	else if (c!= '\n')
		enter();
	
	return virgula=FALSE;
}	

void LeNumeroReal(float *numero)
{
	int ok,virg;
	char c;
	
	ok=scanf("%f",numero);			
	virg=VerificaVirgula(ok);
		
	while(ok==0 || virg==TRUE)
	{
		printf("\nNúmero inválido\n Digite novamente:  ");
		
		ok=scanf("%f",numero);			
		virg=VerificaVirgula(ok);

	}	

}

int VerificaSoNumeros(char *cpf)
{
	int i,SoNumero;
	i=0;
	while((i<TAM_CPF)&&(cpf[i]>= 48)&&(cpf[i]<= 57))
	{
		i++;
	}
	
	if (i==TAM_CPF)
		return SoNumero=TRUE;

	printf("O cpf deve ter só digitos\n");
	return SoNumero=FALSE;

}

int TestaCPF(char *cpf)
{
    int valido;
	if (strlen(cpf)!=TAM_CPF)
	{
		valido=FALSE;
		printf("O cpf deve ter %d digitos\n", TAM_CPF);
		return valido;
	}
	valido=VerificaSoNumeros(cpf);

	return valido;
}

