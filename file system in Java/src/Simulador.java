
public class Simulador {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Depurador dep = new Depurador();
		
		Diretorio d1 = new Diretorio();
		d1.nome = "d3";
		dep.debug(d1);
		dep.debugCall(d1);
		
		Arquivo a1 = new Arquivo();
		a1.nome = "a1";
		d1.getArquivos().add(a1);
		dep.debug(a1);
		dep.debugCall(a1);
		
		Arquivo a2 = new Arquivo();
		a2.nome = "a2";
		d1.getArquivos().add(a2);
		dep.debug(a2);
		dep.debugCall(a2);
		
		Diretorio d2 = new Diretorio();
		d2.nome = "d2";
		dep.debug(d2);
		dep.debugCall(d2);
		
		Diretorio d3 = new Diretorio();
		d3.nome = "d3";
		d3.caminho = "home/";
		d3.getDiretorios();
		d3.getDiretorios();
		d3.incluirItem(d1);
		d3.incluirItem(d2);
		
		Arquivo a3 = new Arquivo();
		a3.nome = "a3";
		d3.getArquivos().add(a3);
		dep.debug(a3);
		dep.debugCall(a3);
		d2.incluirItem(a1);
		
		
		
		dep.debug(d3);
		dep.debugCall(d3);
		
		
	}

}
