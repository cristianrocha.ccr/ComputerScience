import java.lang.reflect.Field;
import java.lang.reflect.Method;


public interface IDepurador {

	public void debug(Field f); // imprime o nome de um atributo

	public void debug(Method m); // imprime o nome e o tipo de um m�todo

	public void debug(Class c); // imprime o nome de uma classe

	public void debugCall(Object o); // imprime o nome e tipo dos m�todos de um objeto e chama os mesmos. Aplicar esta fun��o apenas a m�todos sem par�metros.

	public void debug(Object o); // imprime o nome da classe, o nome dos atributos e os valores dos atributos de um objeto.
	
}
