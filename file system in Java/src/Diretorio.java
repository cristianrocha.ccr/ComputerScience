import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.*;

public class Diretorio {
	public Collection<Arquivo> arquivos;
	public Collection<Diretorio> diretorios;
	public String nome;
	public String caminho;
	
	public void listaArvore(){
		
		int i;
		
		
		
		System.out.println("FILHOS: ");
		System.out.println("Diretorios: ");
		for (Diretorio f : diretorios){
			System.out.println(f.nome + ", ");
		}
		
		System.out.println("Arquivos:");
		for (Arquivo a : arquivos){
			System.out.println(a.nome + ", ");
		}
		for(Diretorio d: diretorios){
		d.listaArvore();
		}
	}
	
	public void excluiItem(Object o){
		//TODO - definir se o usu�rio ir� especificar qual obj ser� exclu�do
	}
	
	public void incluirItem (Diretorio d){
		if (d != null){
			d.caminho = this.caminho + d.nome + "/";
			diretorios.add(d);
		
		}
	}
	public void incluirItem (Arquivo a){
		if (a != null){
			arquivos.add(a);
		}
	}
	
	public Collection<Arquivo> getArquivos() {
		if (arquivos == null){
			setArquivos(new ArrayList<Arquivo>());
		}
		return arquivos;
	}
	public void setArquivos(Collection<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}
	public Collection<Diretorio> getDiretorios() {
		if (diretorios == null){
		
			setDiretorios(new ArrayList<Diretorio>());
		}
		return diretorios;
	}
	public void setDiretorios(Collection<Diretorio> diretorios) {
		this.diretorios = diretorios;
	}
	/*
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCaminho() {
		return caminho;
	}
	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}*/
}
