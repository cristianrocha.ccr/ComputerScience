# 1. Professional Linux Kernel Architecture, Wolfgang Mauererpg. Pg. 329. 
#	 This code ensures that the new (and, as a precaution, the old) value of brk is a 
#	 multiple of the system page size. In other words, a page is the smallest memory 
#	 area that can be reserved with brk.
# 	$ getconf PAGESIZE
#
# pushad
# xor p/ zerar
#-------VAR. GLOBAIS------------------------

	.section .data
lista_dupla: .long 0					# inicio da heap

#-------CONSTANTES-------------------------

	.equ BRK, 45
	.equ SYSCALL, 0x80

	.equ OFFSET_PARAMETRO, 8
	.equ OFFSET_PARAMETRO2, 12
	.equ NULO, 0	

	.equ DISPONIVEL, 1 
	.equ INDISPONIVEL, 0
	.equ CABECALHO, 16
	.equ OFFSET_DISPONIBILIDADE, 12
	.equ OFFSET_TAMBLOCO, 8
	.equ OFFSET_PROX, 4
	.equ OFFSET_ANT, 0
#------------------------------------------

	.section .text

#--------FUNÇÃO MEUALOCAMEM----------------
# void *meuAlocaMem ( int num_bytes );	
#
# %eax - Posição do bloco atual
# %ebx - Bloco anterior
# %ecx - Quantidade bytes a ser alocado
# %edx - Tamanho do bloco atual
#
#------------------------------------------

	.globl meuAlocaMem
	.type meuAlocaMem, @function

msg: .string "%p\n"

meuAlocaMem:


	pushl   %ebp					# Padrão funções
	movl    %esp, %ebp				#

	cmpl $0, lista_dupla
	jne ja_inicializado				# Verifica primeira vez que o malloc é executado
	movl $0, %ebx					#  0 in the %ebx register, indicates that we want the current position of brk space.
	movl $BRK, %eax					#  45 -> sys_brk
	int $SYSCALL					#  chama vetor de interrupções

	movl OFFSET_PARAMETRO(%ebp), %ecx		# pega valor(tamanho) a ser alocado
	incl %eax					# %eax contém o último endereço valido, então pega-se o primeiro livre.  %eax = %eax + 1
	movl %eax, lista_dupla					
   	movl $0, %ebx		
   	jmp abre_espaco

ja_inicializado:

	movl OFFSET_PARAMETRO(%ebp), %ecx		# pega valor(tamanho) a ser alocado
	movl lista_dupla, %eax


loop_procura_espaco:

# ebx é o bloco com menor tamanho maior que o espaco a ser alocado
# edx é o tamanho do bloco atual
# ecx é o endereco do bloco que sera alocado

	pushl %edx					#empilha o %edx
	movl $0,%edx					#inicializa o %edx	
	movl OFFSET_TAMBLOCO(%eax),%edx			# atualiza tamanho do bloco atual
	cmpl $INDISPONIVEL, OFFSET_DISPONIBILIDADE(%eax)# 
	je proximo_bloco				# verifica se bloco esta disponivel
	
	cmpl %ebx,%edx	#verifica se o bloco atual é menor que, até o momento, o bloco a ser alocado
	jg desempilha2	#se for maior continua no loop		

	pushl %eax	#empilha o %eax

	movl OFFSET_PARAMETRO(%ebp), %eax  #coloca em %eax o tamanho do espaco a ser alocado

	cmpl %eax,%edx	# verifica se o tamanho do espaço a ser alocado com o tamanho do bloco atual
	jl desempilha	# se o bloco atual for menor ele continua no loop***
	popl %eax

	

	movl %edx,%ebx #atualiza o tamanho do menor bloco a ser alocado

	movl %eax, %ecx   ## atualiza o endereco do bloco que sera alocado
	
proximo_bloco:	movl OFFSET_PROX(%eax), %eax			# atualiza %eax para o próximo bloco
		cmpl $0,%eax
		je fim_loop_procura_espaco  	# se for igual, fim da heap


desempilha2:
		movl OFFSET_PROX(%eax), %eax
		popl %edx
		jmp loop_procura_espaco

desempilha:
		popl %eax
		jmp loop_procura_espaco	

desempilha3:

		popl %edx
		jmp aloca_bloco_igual


desempilha4:
		popl %edx
	
		jmp aloca_bloco_maior

fim_loop_procura_espaco:


	pushl %edx	#empilha edx
	movl OFFSET_PARAMETRO(%ebp), %edx 	#salva o tamanho a ser alocado em edx

	movl %ecx, %eax  ## atualizando o endereco do bloco encontrado a ser alocado
	cmpl   %edx, %ebx ## compara se o bloco encontrado é do mesmo tamanho que o tamanho a ser alocado
	je desempilha3

	cmpl   %edx,%ebx ## compara se o bloco encontrado é maior que o tamanho a ser alocado 
	jg desempilha4




fim_lista:

	movl OFFSET_ANT(%eax), %ebx
	movl %ebx, %eax
	addl $CABECALHO, %eax
	addl OFFSET_TAMBLOCO(%ebx), %eax

abre_espaco:

	pushl %ebx					# salva o endereço do bloco anterior

	movl %eax, %ebx					#
	addl $CABECALHO, %ebx				#
	addl %ecx, %ebx					# %ebx contem o endereço a ser setado o brk	

	pushl %eax					# salva registradores que serão utilizados no futuro
	pushl %ecx					#
	
	movl $BRK, %eax					#			
	int $SYSCALL					# requisita novo BRK
	
	cmpl $0, %eax					# verifica se sucedido
	je erro						#
	
	popl %ecx					# restaura registradores
	popl %eax					#	
	popl %ebx					#	

	cmpl $0, %ebx
	je sem_bloco_anterior 
	movl %eax, OFFSET_PROX(%ebx)			# bloco -> ant -> prox = bloco atual

sem_bloco_anterior:

	movl %ebx , OFFSET_ANT(%eax)			# seta o bloco anterior
	movl $0 , OFFSET_PROX(%eax)			# seta o próximo bloco como NULL
	movl $INDISPONIVEL, OFFSET_DISPONIBILIDADE(%eax)# seta bloco como indisponível
	movl %ecx, OFFSET_TAMBLOCO(%eax)		# seta o tamanho do bloco

	addl $CABECALHO, %eax 				# %eax contem o endereço de retorno (ponteiro para o endereço do espaço da memória que foi alocado)

	popl %ebp					#
	ret						# retorna da função

erro:

	movl $0, %eax					# função retorna endereço 0 em caso de erro

	popl %ebp					#
	ret						# retorna da função

aloca_bloco_igual:

	movl $INDISPONIVEL, OFFSET_DISPONIBILIDADE(%eax)# seta bloco como indisponível
	addl $CABECALHO, %eax				# endereco de retorno

	popl %ebp					#
	ret						# retorna da função

aloca_bloco_maior:

	subl $CABECALHO, %edx				# Verifica se o tamanho do bloco subtraido da quantidade a ser alocada 
	cmpl %ecx, %edx					# terá tem pelos menos 16 + 1 bytes
	jle  proximo_bloco				#

	movl %eax,%ebx	

	subl %ecx, %edx					# edx contem o tamanho do segundo bloco
	
	addl  $CABECALHO,%ebx
	addl  %ecx,%ebx

	movl $INDISPONIVEL, OFFSET_DISPONIBILIDADE(%eax)# seta bloco como indisponível
	movl %ecx, OFFSET_TAMBLOCO(%eax)		# atualiza o tamanho do bloco
	movl OFFSET_PROX(%eax), %ecx			# ecx contem bloco->prox antes de o bloco ser fragmentado próximo bloco - Salvar pq ele vai ser substituido
	movl %ebx, OFFSET_PROX(%eax)			# atualiza bloco->prox
	movl %eax , OFFSET_ANT(%ebx)			# seta o bloco anterior
	movl %ecx , OFFSET_PROX(%ebx)			# seta o próximo bloco como NULL
	movl $DISPONIVEL, OFFSET_DISPONIBILIDADE(%ebx)  # seta bloco como disponível
	movl %edx, OFFSET_TAMBLOCO(%ebx)		# atualiza o tamanho do bloco

	addl $CABECALHO, %eax				# valor de retorno da função deve ser o bloco anterior

	popl %ebp					#
	ret						# retorna da função
#------------------------------------------

#--------FUNÇÃO IMPRMAPA-------------------
# void  imprMapa ( );
# %ecx - i
#------------------------------------------
#
	.globl imprMapa
	.type imprMapa, @function

msg1: .string "######## %p ########\n bloco %d\n tamanho: %d\n status: %d\n prox: %p\n ant: %p\n######## %p ########\n"
msg2: .string"Inicio heap: %p\n\n"

imprMapa:

	pushl   %ebp
	movl    %esp,%ebp

	pushl lista_dupla
	pushl  $msg2
	call   printf
	addl   $8, %esp

	movl    $1,%ecx
	movl    lista_dupla, %eax

loop_impr:

	cmpl    $0, %eax
	je     fim

	movl    %eax,%ebx
	addl    $CABECALHO,%ebx
	addl    OFFSET_TAMBLOCO(%eax),%ebx
	

	pushl   %ebx
	pushl  OFFSET_ANT(%eax)
	pushl  OFFSET_PROX(%eax)
	pushl  OFFSET_DISPONIBILIDADE(%eax)
	pushl  OFFSET_TAMBLOCO(%eax)
	pushl   %ecx
	pushl   %eax
	pushl   $msg1
	call   printf
	addl    $4,%esp
	popl    %eax
	popl    %ecx
	addl    $20,%esp

	movl   OFFSET_PROX(%eax),%eax
	incl    %ecx
	jmp    loop_impr

fim:

	popl    %ebp
	ret    

#------------------------------------------

#--------FUNÇÃO IMPRMAPAVISUAL-------------------
# void  imprMapaVisual ( );
#------------------------------------------
#
	.globl imprMapaVisual
	.type imprMapaVisual, @function

mensagem1: .string"\n\n Início visualização do mapa de memória . . . visualizacao de UM SIMBOLO para cada 10 BYTES \n\n"
mensagem2: .string"*"
mensagem3: .string"-"
mensagem4: .string"\n"

imprMapaVisual:

	pushl   %ebp
	movl    %esp,%ebp

	pushl  lista_dupla
	pushl  $mensagem1
	call   printf
	addl   $8, %esp

	movl   lista_dupla, %eax

loop_IMPRIME_MAPA:
	cmpl   $0, %eax
	je     fim_imprMapaVisual

	movl    OFFSET_TAMBLOCO(%eax),%ebx
	
	movl    OFFSET_DISPONIBILIDADE(%eax), %edx
	
	movl    OFFSET_PROX(%eax),%eax

loop_imprime_caracter:

	cmpl    $0, %ebx
	jle     fim_loop_imprime_caracter
	
	cmpl    $0, %edx
	je      ocupado
	
	pushl  %edx
	pushl  %ebx
	pushl  %eax
	pushl  $mensagem3
	call   printf
	addl   $4, %esp
	popl   %eax
	popl   %ebx
	popl   %edx
	
	jmp     fim_ocupado
	
ocupado: 

	pushl  %edx
	pushl  %ebx
	pushl  %eax
	pushl  $mensagem2
	call   printf
	addl   $4, %esp
	popl   %eax
	popl   %ebx
	popl   %edx
	
fim_ocupado:
	subl $10, %ebx                 # ira imprimir um simbolo para cada 10 bytes
	
	jmp loop_imprime_caracter
	
fim_loop_imprime_caracter:
	
	pushl  %edx
	pushl  %ebx
	pushl  %eax
	pushl  $mensagem4
	call   printf
	addl   $4,%esp
	popl   %eax
	popl   %ebx
	popl   %edx

	jmp    loop_IMPRIME_MAPA

	
fim_imprMapaVisual:
	
	pushl  %edx
	pushl  %ebx
	pushl  %eax
	pushl  $mensagem4
	call   printf
	addl   $4,%esp
	popl   %eax
	popl   %ebx
	popl   %edx
	
	popl    %ebp
	ret    

#------------------------------------------

#--------FUNÇÃO MEULIBERAMEM---------------
# void  meuLiberaMem ( void*  ptr );
# 
#------------------------------------------
#
	.globl meuLiberaMem
	.type meuLiberaMem, @function

meuLiberaMem:

	pushl  %ebp
	movl   %esp,%ebp
	movl   OFFSET_PARAMETRO(%ebp),%eax
	subl   $CABECALHO,%eax
	movl   OFFSET_ANT(%eax),%ebx
	movl   OFFSET_PROX(%eax),%ecx
	cmpl   $INDISPONIVEL,%ebx
	je     sem_bloco_ant
	cmpl   $DISPONIVEL,OFFSET_DISPONIBILIDADE(%ebx)
	jne    sem_bloco_ant
	cmpl   $INDISPONIVEL,%ecx
	jne    prox_nao_nulo

	# 	CASO 1: TEM ANTERIOR DISPONIVEL E NÃO TEM PRÓXIMO 
	movl   %ebx,%eax
	jmp    diminui_brk

prox_nao_nulo:
	#	CASO 2: TEM ANTERIOR DISPONIVEL E TEM PRÓXIMO DISPONIVEL
	# IF (PROX->DISPONIVEL) 
	cmpl   $DISPONIVEL,OFFSET_DISPONIBILIDADE(%ecx)		
	jne    prox_nao_dispo

	movl   OFFSET_PROX(%ecx),%edx

	# IF(PROX->PROX==NULL)
	cmpl   INDISPONIVEL,%edx
	jne    prox_dispo_com_prox

	movl   %ebx,%eax
	jmp    diminui_brk

prox_dispo_com_prox:

	#	CASO 3: TEM ANTERIOR DISPONIVEL E TEM PRÓXIMO DISPONIVEL E PRÓXIMO->PRÓXIMO INDISPONIVEL

	movl  OFFSET_TAMBLOCO(%eax),%edx
	addl $CABECALHO,%edx
	addl OFFSET_TAMBLOCO(%ebx),%edx
	addl OFFSET_TAMBLOCO(%ecx),%edx
	addl $CABECALHO,%edx
	movl   %ebx,%eax
	movl   %ecx,%ebx
	movl   %edx,%ecx
	jmp    uniao

prox_nao_dispo:

	#	CASO 4: TEM ANTERIOR DISPONIVEL E TEM PRÓXIMO INDISPONIVEL


	movl   OFFSET_TAMBLOCO(%eax),%edx
	addl $CABECALHO,%edx
	addl OFFSET_TAMBLOCO(%ebx),%edx
	movl   %eax,%ecx
	movl   %ebx,%eax
	movl   %ecx,%ebx
	movl   %edx,%ecx
	jmp    uniao

sem_bloco_ant:

	# IF(PROX==NULL)
	#	CASO 5:  NAO TEM ANTERIOR E NAO TEM PROXIMO
	cmpl   $NULO,%ecx
	jne    sem_ant_prox_nao_nulo
	movl   %eax,%ebx
	jmp    diminui_brk

sem_ant_prox_nao_nulo:

	# IF(PROX->DISPONIVEL)
	#	CASO 6
	cmpl   $DISPONIVEL,OFFSET_DISPONIBILIDADE(%ecx)
	jne    sem_ant_prox_nao_dispo
	movl   OFFSET_PROX(%ecx),%edx
	# IF(PROX->PROX==NULL)
	cmpl   $INDISPONIVEL,%edx
	jne    sem_ant_prox_dispo_com_prox

	#	CASO 6: NAO TEM ANTERIOR E  TEM PROXIMO DISPONIVEL	
		
	movl   %eax,%ebx
	jmp    diminui_brk

sem_ant_prox_dispo_com_prox:

	#	CASO 7: NAO TEM ANTERIOR E TEM PROXIMO DISPONIVEL E PROXIMO->PROXIMO INDISPONIVEL

	movl   OFFSET_TAMBLOCO(%ecx),%edx
	addl $CABECALHO,%edx
	addl OFFSET_TAMBLOCO(%eax),%edx
	movl   %ecx,%ebx
	movl   %edx,%ecx
	jmp    uniao

sem_ant_prox_nao_dispo:


	#	CASO 8: NAO TEM ANTERIOR E TEM PROXIMO NAO DISPONIVEL

	movl   $DISPONIVEL,OFFSET_DISPONIBILIDADE(%eax)
	jmp    fim_free

diminui_brk:

	pushl  %eax
	movl   $BRK,%eax
	int    $0x80
	popl   %eax
	movl   OFFSET_ANT(%eax),%ebx

	cmpl	%eax, lista_dupla
	jne	continua
	movl $NULO, lista_dupla 	

continua:	

	cmpl   $NULO,%ebx
	je     fim
	movl   $NULO,OFFSET_PROX(%ebx)
	jmp    fim_free

uniao:

	movl   %ecx,OFFSET_TAMBLOCO(%eax)
	movl   $DISPONIVEL,OFFSET_DISPONIBILIDADE(%eax)
	movl   OFFSET_PROX(%ebx),%ecx
	movl   %ecx,OFFSET_PROX(%eax)
	cmpl   $NULO,%ecx
	je     fim_free
	movl   %eax,OFFSET_ANT(%ecx)

fim_free:

	popl   %ebp
	ret    

#----------------------------------------------------------

	.globl meuCalocaMem
	.type meuCalocaMem, @function

meuCalocaMem:

	pushl %ebp
	movl %esp, %ebp
	
	movl OFFSET_PARAMETRO(%ebp), %eax
	mull OFFSET_PARAMETRO2(%ebp)

	pushl %eax					# passagem parâmetro
	call meuAlocaMem				# aloca memória	
	popl %ebx					# recupera num. bytes a serem alocados
	push %eax					# empilha endereço de retorno da função
	
	movl $1, %ecx					# i = 1

loop:
	movb $0, (%eax)					# zera 1 byte do endereço
	incl %eax					
	incl %ecx					# i++
	cmpl %ecx, %ebx			
	jle loop					# while ( i <= %ebx )

	popl %eax
	popl %ebp
	ret

#----------------------------------------------------------

	.globl meuRealocaMem
	.type meuRealocaMem, @function

msg52: .string"%p\n %p\n"

meuRealocaMem:

	pushl %ebp
	movl %esp, %ebp

	pushl OFFSET_PARAMETRO2(%ebp)
	call meuAlocaMem
	addl $4, %esp
	pushl %eax

	movl OFFSET_PARAMETRO(%ebp), %ebx
	movl -OFFSET_TAMBLOCO(%ebx), %ecx

	addl %ebx, %ecx

loop_realoc:

	cmpl %ecx, %ebx
	jge fim_realoc


	movb (%ebx), %edx
	movb %edx, (%eax)
	incl %ebx
	incl %eax

	jmp loop_realoc

fim_realoc:

	push OFFSET_PARAMETRO(%ebp)
	call meuLiberaMem
	addl $4, %esp
	
	popl %eax
	popl %ebp
	ret
	

#----------------------------------------------------------
